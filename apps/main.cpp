#define vbswy_cxx
#define DAOD_tree_cxx

#include <DAOD_tree.h>
#include <vbswy.h>
#include <iostream>
#include <fstream>
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include "AtlasLabels.h"
using namespace std;

Double_t ScaleFactor(TString f_name){
    TFile *large_file = TFile::Open(f_name,"READ"); //TFile* myfile = new TFile("","OPEN");

    TTree *fChain_DAOD=(TTree*)large_file->Get("DAOD_tree");
    DAOD_tree large_DAOD(fChain_DAOD);//  lage_DAOD.Loop();

    Long64_t nentries_DAOD = fChain_DAOD->GetEntriesFast();
    Double_t lumi=0, xsec=0, geff=0, kfac=0, sw=0;
    for (Long64_t jentry=0; jentry<nentries_DAOD;jentry++) {
        fChain_DAOD->GetEntry(jentry);
        if (jentry==0) {
            lumi = large_DAOD.luminosity;
            xsec = large_DAOD.xsection;
            geff = large_DAOD.geneff;
            kfac = large_DAOD.kfactor;
        }
        sw += large_DAOD.sum_of_weights;
    }
    return lumi*xsec*geff*kfac/sw;
}

int main() {
    std::cout << "Hello, World!" << std::endl;
    ifstream file("/lustre/collider/wangxi/data_dijet/v6_mini/v6_mini_list2.txt");
    TH1D *h_mjj = new TH1D("h_mjj", "", 50, 500, 6000 );
    TH1D *h_mwy = new TH1D("h_mwy", "", 50, 500, 6000 );
    TH1D *h_signed_dphi = new TH1D("h_signed_dphi", "", 50, -3, 3 );
    TH1D *h_dyjj = new TH1D("h_dyjj", "", 50, 0, 10 );
    while(!file.eof()) {
        TString dir_file;
        file >> dir_file;
        if (dir_file == "") continue;
        TString f_name = string(dir_file).substr(string(dir_file).find_last_of("/") + 1);
        cout << f_name << endl;

        Double_t sf = ScaleFactor(dir_file);

        TFile *large_file = TFile::Open(dir_file, "READ"); //TFile* myfile = new TFile("","OPEN");

        TTree *fChain_vbswy;
        fChain_vbswy = (TTree *) large_file->Get("vbswy");
        vbswy vbswy(fChain_vbswy); // large_vbswy.Loop();

        Long64_t nentries_vbswy = fChain_vbswy->GetEntries();         //change 1
        cout << "vbswy tree nentries: " << nentries_vbswy << endl;

        for (Long64_t jentry = 0; jentry < nentries_vbswy; jentry++) {
            fChain_vbswy->GetEntry(jentry);
            Double_t mc_weight = sf * vbswy.weight_all * vbswy.weight_m * vbswy.weight_e;    //

            if ( (vbswy.pass_baseline_e || vbswy.pass_baseline_mu ) && vbswy.jj_drap>2 && (vbswy.wy_xi<=0.35 && vbswy.n_gapjets==0 && !vbswy.isBlind) ){
                h_mjj->Fill(vbswy.jj_m, mc_weight);
                h_mwy->Fill(vbswy.wy_m, mc_weight);
                h_signed_dphi->Fill(vbswy.jj_dphi_signed, mc_weight);
                h_dyjj->Fill(vbswy.jj_drap, mc_weight);
            }


        }
    }
    SetAtlasStyle();
    TCanvas *c1= new TCanvas("c1", "", 800, 600);
    c1->cd();
    h_mjj->Draw();
    h_mjj->GetXaxis()->SetTitle("M_{jj}");
    c1->SaveAs("bkg_mjj.png");

    TCanvas *c2= new TCanvas("c2", "", 800, 600);
    c2->cd();
    h_mwy->Draw();
    h_mwy->GetXaxis()->SetTitle("M_{Wy}");
    c2->SaveAs("bkg_mwy.png");

    TCanvas *c3= new TCanvas("c3", "", 800, 600);
    c3->cd();
    h_signed_dphi->Draw();
    h_signed_dphi->GetXaxis()->SetTitle("Signed_Dphi_{jj}");
    c3->SaveAs("bkg_signed_dphi.png");

    TCanvas *c4= new TCanvas("c4", "", 800, 600);
    c4->cd();
    h_dyjj->Draw();
    h_dyjj->GetXaxis()->SetTitle("Dy_{jj}");
    c4->SaveAs("bkg_dyjj.png");
    return 0;
}
