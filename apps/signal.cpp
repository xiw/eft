#define vbswy_cxx
#define DAOD_tree_cxx

#include <DAOD_tree.h>
#include <vbswy.h>
#include <iostream>
#include <fstream>
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include "AtlasLabels.h"


#include "stdio.h"
#include "stdlib.h"
#include "map"
using namespace std;
int main() {
    map<TString, TH1D *> map_hist;
    vector<TH1D *> vhist_m_iq;
    vector<TH1D *> vhist_t_iq;
    TString dir="/lustre/collider/wangzhen/atlas/vbs/eft/eftsignal";
    for (int i=0; i<8; i++){
        if((access(dir+ "/M" + to_string(i) + "_INT_hists/hist-rootfiles.root", 0)) != -1 ) {
            TFile *f_mint = TFile::Open(dir + "/M" + to_string(i) + "_INT_hists/hist-rootfiles.root", "READ");
            TFile *f_mquad = TFile::Open(dir + "/M" + to_string(i) + "_QUAD_hists/hist-rootfiles.root", "READ");
            map_hist.insert(std::pair<TString, TH1D *>("M" + to_string(i) + "_INT", (TH1D *) f_mint->Get("h_mjj")));
            map_hist.insert(std::pair<TString, TH1D *>("M" + to_string(i) + "_QUAD", (TH1D *) f_mquad->Get("h_mjj")));
            cout<<i<<endl;
        }
        if((access(dir+ "/T" + to_string(i) + "_INT_hists/hist-rootfiles.root", 0)) != -1 ){
            TFile *f_tint = TFile::Open(dir+ "/T" + to_string(i) + "_INT_hists/hist-rootfiles.root", "READ");
            TFile *f_tquad = TFile::Open(dir+ "/T" + to_string(i) + "_QUAD_hists/hist-rootfiles.root", "READ");
            map_hist.insert(std::pair<TString, TH1D *>("T" + to_string(i) + "_INT", (TH1D *) f_tint->Get("h_mjj")));
            map_hist.insert(std::pair<TString, TH1D *>("T" + to_string(i) + "_QUAD", (TH1D *) f_tquad->Get("h_mjj")));
            cout<<i<<endl;
        }
    }

    //r (int i=0; i<8; i++){
    //  vhist_m_iq.push_back(vhist_mint.at(i)->Add(vhist_mquad.at(i)));
    //  vhist_t_iq.push_back(vhist_tint.at(i)->Add(vhist_tquad.at(i)));
    //

    SetAtlasStyle();
    TCanvas *c1= new TCanvas("c1", "", 800, 600);
    c1->cd();
    //ut<<vhist_mint.size()<<endl;
    for(std::map<TString,TH1D* >::iterator it = map_hist.begin(); it != map_hist.end(); ++it) {
        cout<<it->first<<endl;
        static Int_t i =1;
        if (it->first.BeginsWith("M") && it->first.Contains("INT")) {
            it->second->Draw("same");
            it->second->SetLineColor(i + 1); i++;
        }
        //ist_mint.at(i)->Draw("same");
        //ist_mint.at(i)->SetLineColor(i+1);
    }
    //ist_mint.at(0)->GetXaxis()->SetTitle("M_{jj}");
    c1->SaveAs("sig_mjj_mint.png");
/*
    TCanvas *c2= new TCanvas("c2", "", 800, 600);
    c2->cd();
    cout<<vhist_mquad.size()<<endl;
    for(int i=0;i<vhist_mquad.size();i++){
        vhist_mquad.at(i)->Draw("same");
        vhist_mquad.at(i)->SetLineColor(i+1);
    }
    vhist_mquad.at(0)->GetXaxis()->SetTitle("M_{jj}");
    c2->SaveAs("sig_mjj_mquad.png");

    TCanvas *c3= new TCanvas("c3", "", 800, 600);
    c3->cd();
    cout<<vhist_tint.size()<<endl;
    for(int i=0;i<vhist_tint.size();i++){
        vhist_tint.at(i)->Draw("same");
        vhist_tint.at(i)->SetLineColor(i+1);
    }
    vhist_tint.at(0)->GetXaxis()->SetTitle("M_{jj}");
    c3->SaveAs("sig_mjj_tint.png");

    TCanvas *c4= new TCanvas("c4", "", 800, 600);
    c4->cd();
    cout<<vhist_tquad.size()<<endl;
    for(int i=0;i<vhist_tquad.size();i++){
        vhist_tquad.at(i)->Draw("same");
        vhist_tquad.at(i)->SetLineColor(i+1);
    }
    vhist_tquad.at(0)->GetXaxis()->SetTitle("M_{jj}");
    c4->SaveAs("sig_mjj_tquad.png");
*/
 /*
    TCanvas *c2= new TCanvas("c2", "", 800, 600);
    c2->cd();
    h_mwy->Draw();
    h_mwy->GetXaxis()->SetTitle("M_{Wy}");
    c2->SaveAs("bkg_mwy.png");

    TCanvas *c3= new TCanvas("c3", "", 800, 600);
    c3->cd();
    h_signed_dphi->Draw();
    h_signed_dphi->GetXaxis()->SetTitle("Signed_Dphi_{jj}");
    c3->SaveAs("bkg_signed_dphi.png");

    TCanvas *c4= new TCanvas("c4", "", 800, 600);
    c4->cd();
    h_dyjj->Draw();
    h_dyjj->GetXaxis()->SetTitle("Dy_{jj}");
    c4->SaveAs("bkg_dyjj.png");
    */
    return 0;
}
