//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Dec  6 01:07:48 2021 by ROOT version 6.20/00
// from TTree DAOD_tree/TTree for lumi, XS and Sum_of_Weights
// found on file: user.jmcgowan.vbswy-tree-v06-run1.700016.mc16a.p4097_MxAOD.root
//////////////////////////////////////////////////////////

#ifndef DAOD_tree_h
#define DAOD_tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
using namespace std;

class DAOD_tree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        luminosity;
   Double_t        xsection;
   Double_t        geneff;
   Double_t        kfactor;
   Double_t        xsbrfiltereff;
   Double_t        sum_of_weights;
   vector<string>  *th_weightnames;

   // List of branches
   TBranch        *b_luminosity;   //!
   TBranch        *b_xsection;   //!
   TBranch        *b_geneff;   //!
   TBranch        *b_kfactor;   //!
   TBranch        *b_xsbrfiltereff;   //!
   TBranch        *b_sum_of_weights;   //!
   TBranch        *b_th_weightnames;   //!

   DAOD_tree(TTree *tree=0);
   virtual ~DAOD_tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   //virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef DAOD_tree_cxx
DAOD_tree::DAOD_tree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.jmcgowan.vbswy-tree-v06-run1.700016.mc16a.p4097_MxAOD.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.jmcgowan.vbswy-tree-v06-run1.700016.mc16a.p4097_MxAOD.root");
      }
      f->GetObject("DAOD_tree",tree);

   }
   Init(tree);
}

DAOD_tree::~DAOD_tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DAOD_tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DAOD_tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DAOD_tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   th_weightnames = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("luminosity", &luminosity, &b_luminosity);
   fChain->SetBranchAddress("xsection", &xsection, &b_xsection);
   fChain->SetBranchAddress("geneff", &geneff, &b_geneff);
   fChain->SetBranchAddress("kfactor", &kfactor, &b_kfactor);
   fChain->SetBranchAddress("xsbrfiltereff", &xsbrfiltereff, &b_xsbrfiltereff);
   fChain->SetBranchAddress("sum_of_weights", &sum_of_weights, &b_sum_of_weights);
   fChain->SetBranchAddress("th_weightnames", &th_weightnames, &b_th_weightnames);
   Notify();
}

Bool_t DAOD_tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DAOD_tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DAOD_tree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef DAOD_tree_cxx
