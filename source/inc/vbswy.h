//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Dec  6 01:07:28 2021 by ROOT version 6.20/00
// from TTree vbswy/TTree for the VBSWgamma analysis
// found on file: user.jmcgowan.vbswy-tree-v06-run1.700016.mc16a.p4097_MxAOD.root
//////////////////////////////////////////////////////////

#ifndef vbswy_h
#define vbswy_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
using namespace std;
class vbswy {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          runnumber;
   ULong64_t       eventnumber;
   UInt_t          dsid;
   Double_t        weight_all;
   Double_t        weight_mc;
   Double_t        weight_prw;
   Double_t        weight_gam;
   Double_t        weight_e;
   Double_t        weight_m;
   Double_t        weight_vertex;
   vector<double>  *th_syst_weights;
   Double_t        e_trigger_sf;
   Double_t        mu_trigger_sf;
   Int_t           Wy2jEventType;
   Int_t           Wy2jEventFSPartN;
   vector<int>     *fs_prt_id;
   vector<float>   *fs_prt_mass;
   Int_t           pass_all;
   Int_t           pass_init;
   Int_t           pass_trigger;
   Int_t           pass_primary_vertex;
   Int_t           n_primary_vertex;
   Int_t           pass_dq;
   Int_t           pass_duplicate;
   Int_t           pass_jetclean;
   Int_t           pass_onelep;
   Int_t           pass_metcut;
   Int_t           pass_onegam;
   Int_t           pass_twojet;
   Int_t           pass_mjj;
   Int_t           pass_second_lepton_veto;
   Int_t           pass_ly_Z_veto;
   Int_t           pass_dphi;
   Int_t           pass_dr;
   Int_t           pass_baseline_e;
   Int_t           pass_baseline_mu;
   Int_t           pass_reco;
   Int_t           isBlind;
   Int_t           pass_onelep_truth;
   Int_t           pass_onegam_truth;
   Int_t           pass_trigger_pt_truth;
   Int_t           pass_ly_Z_veto_truth;
   Int_t           pass_twojet_truth;
   Int_t           pass_dr_truth;
   Int_t           pass_dphi_truth;
   Int_t           pass_baseline_e_truth;
   Int_t           pass_baseline_mu_truth;
   Int_t           pass_truth;
   Int_t           pass_cra_y;
   Int_t           pass_cra_ly;
   Int_t           pass_cra_wy;
   Int_t           pass_crb_y;
   Int_t           pass_crb_ly;
   Int_t           pass_crb_wy;
   Int_t           pass_crc_y;
   Int_t           pass_crc_ly;
   Int_t           pass_crc_wy;
   Int_t           pass_sr_y;
   Int_t           pass_sr_ly;
   Int_t           pass_sr_wy;
   Int_t           pass_cra_y_truth;
   Int_t           pass_cra_ly_truth;
   Int_t           pass_cra_wy_truth;
   Int_t           pass_crb_y_truth;
   Int_t           pass_crb_ly_truth;
   Int_t           pass_crb_wy_truth;
   Int_t           pass_crc_y_truth;
   Int_t           pass_crc_ly_truth;
   Int_t           pass_crc_wy_truth;
   Int_t           pass_sr_y_truth;
   Int_t           pass_sr_ly_truth;
   Int_t           pass_sr_wy_truth;
   Double_t        n_bjets_truth;
   Double_t        j0gam_dr_truth;
   Double_t        j1gam_dr_truth;
   Double_t        j0lep_dr_truth;
   Double_t        j1lep_dr_truth;
   Double_t        j0met_dphi_truth;
   Double_t        j1met_dphi_truth;
   Double_t        lepgam_dr_truth;
   Int_t           is_Wenu;
   Int_t           is_Wmunu;
   Int_t           is_Wenu_truth;
   Int_t           is_Wmunu_truth;
   Double_t        pv_z;
   Int_t           lep_n;
   Int_t           lep_n_truth;
   Double_t        lep_truth_pt;
   Double_t        lep_truth_eta;
   Double_t        lep_pt;
   Double_t        lep_eta;
   Double_t        lep_phi;
   Double_t        lep_truth_phi;
   Double_t        lep_e;
   Int_t           lep_truth_type;
   Int_t           lep_truth_origin;
   Double_t        lep_charge;
   Bool_t          lep_truth;
   Double_t        lep2_pt;
   Double_t        lep2_eta;
   Double_t        lep2_phi;
   Double_t        lep2_e;
   Double_t        lep2_charge;
   Bool_t          lep2_same_flav;
   Int_t           e_n;
   Double_t        e_pt;
   Double_t        e_eta;
   Double_t        e_phi;
   Double_t        e_e;
   Double_t        e_charge;
   Double_t        e2_pt;
   Double_t        e2_eta;
   Double_t        e2_phi;
   Double_t        e2_e;
   Double_t        e2_charge;
   Double_t        e_truth_pt;
   Double_t        e_truth_eta;
   Double_t        e_truth_phi;
   Double_t        e_truth_e;
   Double_t        e_truth_charge;
   Double_t        e2_truth_pt;
   Double_t        e2_truth_eta;
   Double_t        e2_truth_phi;
   Double_t        e2_truth_e;
   Double_t        e2_truth_charge;
   Int_t           mu_n;
   Double_t        mu_pt;
   Double_t        mu_eta;
   Double_t        mu_phi;
   Double_t        mu_e;
   Double_t        mu_charge;
   Double_t        mu2_pt;
   Double_t        mu2_eta;
   Double_t        mu2_phi;
   Double_t        mu2_e;
   Double_t        mu2_charge;
   Double_t        mu_truth_pt;
   Double_t        mu_truth_eta;
   Double_t        mu_truth_phi;
   Double_t        mu_truth_e;
   Double_t        mu_truth_charge;
   Double_t        mu2_truth_pt;
   Double_t        mu2_truth_eta;
   Double_t        mu2_truth_phi;
   Double_t        mu2_truth_e;
   Double_t        mu2_truth_charge;
   Double_t        j0_truth_pt;
   Double_t        j0_truth_rap;
   Double_t        j0_truth_eta;
   Double_t        j0_truth_phi;
   Double_t        j0_truth_e;
   Double_t        j1_truth_pt;
   Double_t        j1_truth_rap;
   Double_t        j1_truth_eta;
   Double_t        j1_truth_phi;
   Double_t        j1_truth_e;
   Double_t        jj_truth_m;
   Double_t        jj_truth_drap;
   Double_t        jj_truth_deta;
   Double_t        jj_truth_dphi;
   Double_t        jj_truth_dr;
   Double_t        truth_ngapjets;
   Int_t           e_truth_type;
   Int_t           e_truth_origin;
   Int_t           e_bc;
   Int_t           lep2_truth_type;
   Int_t           lep2_truth_origin;
   Double_t        met_x;
   Double_t        met_y;
   Double_t        met_phi;
   Double_t        met;
   Double_t        met_eta;
   Double_t        met_truth;
   Double_t        met_x_truth;
   Double_t        met_y_truth;
   Double_t        met_truth_phi;
   Double_t        w_pt;
   Double_t        w_eta;
   Double_t        w_phi;
   Double_t        w_e;
   Double_t        w_m;
   Double_t        w_mt;
   Double_t        w_xi;
   Double_t        w_from_MET_truth_pt;
   Double_t        w_from_MET_truth_eta;
   Double_t        w_from_MET_truth_mT;
   Double_t        w_from_MET_truth_rap;
   Double_t        w_from_MET_truth_E;
   Double_t        w_from_MET_truth_phi;
   Double_t        w_from_MET_truth_xi;
   Double_t        wy_phi;
   Double_t        wy_rap;
   Double_t        wy_m;
   Double_t        wy_pt;
   Double_t        wy_xi;
   Double_t        wy_from_MET_truth_pt;
   Double_t        wy_from_MET_truth_eta;
   Double_t        wy_from_MET_truth_rap;
   Double_t        wy_from_MET_truth_E;
   Double_t        wy_from_MET_truth_phi;
   Double_t        wy_from_MET_truth_xi;
   Double_t        ly_xi;
   Double_t        ly_truth_xi;
   Int_t           gam_n;
   Double_t        gam_pt;
   Double_t        gam_eta;
   Double_t        gam_phi;
   Double_t        gam_e;
   Double_t        gam_xi;
   Int_t           gam_truth_type;
   Int_t           gam_truth_origin;
   Int_t           gam_truth_type_condensed;
   Double_t        gam_truth_pt;
   Double_t        gam_truth_eta;
   Double_t        gam_truth_phi;
   Double_t        gam_truth_e;
   Double_t        gam_truth_xi;
   Bool_t          gam_isLoosePrime2;
   Bool_t          gam_isLoosePrime3;
   Bool_t          gam_idLoose;
   Bool_t          gam_isLoosePrime4a;
   Bool_t          gam_isLoosePrime5;
   Double_t        gam_topoETcone20;
   Double_t        gam_topoETcone40;
   Double_t        gam_pTcone20;
   Bool_t          gam_idTight;
   Bool_t          gam_isoTight;
   Bool_t          gam_isoLoose;
   Int_t           gam_convtype;
   Double_t        gam_convtrk1nSCThits;
   Double_t        gam_convtrk2nSCThits;
   Double_t        gam_convtrk1nPixhits;
   Double_t        gam_convtrk2nPixhits;
   Double_t        gam_convR;
   Double_t        gam_convz;
   Double_t        gam_truthconvR;
   Double_t        gam_pointz;
   Double_t        gam_pointdz;
   Double_t        gam_sf_ideff;
   Double_t        gam_sf_ideff_unc;
   Double_t        gam_sf_isoeff;
   Double_t        gam_ecalib_ratio;
   Double_t        gam_relereso;
   Double_t        gam2_pt;
   Double_t        gam2_eta;
   Double_t        gam2_phi;
   Double_t        gam2_e;
   Double_t        gam2_xi;
   Int_t           gam2_truth_type;
   Int_t           gam2_truth_origin;
   Int_t           gam2_truth_type_condensed;
   Double_t        gam2_truth_pt;
   Double_t        gam2_truth_eta;
   Double_t        gam2_truth_phi;
   Double_t        gam2_truth_e;
   Int_t           gam2_bc;
   Bool_t          gam2_isLoosePrime2;
   Bool_t          gam2_isLoosePrime3;
   Bool_t          gam2_idLoose;
   Bool_t          gam2_isLoosePrime4a;
   Bool_t          gam2_isLoosePrime5;
   Double_t        gam2_topoETcone20;
   Double_t        gam2_topoETcone40;
   Double_t        gam2_pTcone20;
   Bool_t          gam2_idTight;
   Bool_t          gam2_isoTight;
   Bool_t          gam2_isoLoose;
   Int_t           gam2_convtype;
   Double_t        gam2_convtrk1nSCThits;
   Double_t        gam2_convtrk2nSCThits;
   Double_t        gam2_convtrk1nPixhits;
   Double_t        gam2_convtrk2nPixhits;
   Double_t        gam2_convR;
   Double_t        gam2_convz;
   Double_t        gam2_truthconvR;
   Double_t        gam2_pointz;
   Double_t        gam2_pointdz;
   Bool_t          pass_vy_OR;
   Double_t        e_topoetcone20;
   Double_t        e_pTcone20;
   Double_t        e_pTvarcone30;
   Double_t        e_pTcone20_pt1000;
   Double_t        e_pTcone20_pt500;
   Double_t        e_pTvarcone30_pt1000;
   Double_t        e_pTvarcone30_pt500;
   Double_t        e_neflowisol20;
   Double_t        e_idLoose;
   Double_t        e_idMedium;
   Double_t        e_idTight;
   Double_t        e_isoTight;
   Double_t        e_isoLoose;
   Double_t        e_isoFCTight;
   Double_t        e_isoFCLoose;
   Double_t        e_isoPLVTight;
   Double_t        e_isoPLVLoose;
   Double_t        e2_idLoose;
   Double_t        e2_idMedium;
   Double_t        e2_idTight;
   Double_t        e2_isoTight;
   Double_t        e2_isoLoose;
   Double_t        e2_isoFCTight;
   Double_t        e2_isoFCLoose;
   Double_t        e2_isoPLVTight;
   Double_t        e2_isoPLVLoose;
   Bool_t          e_passIP;
   Bool_t          e2_passIP;
   Double_t        e_d0sig;
   Double_t        e_z0;
   Double_t        e2_d0sig;
   Double_t        e2_z0;
   Double_t        mu_topoetcone20;
   Double_t        mu_pTcone20;
   Double_t        mu_pTvarcone30;
   Double_t        mu_pTcone20_pt1000;
   Double_t        mu_pTcone20_pt500;
   Double_t        mu_pTvarcone30_pt1000;
   Double_t        mu_pTvarcone30_pt500;
   Double_t        mu_neflowisol20;
   Bool_t          mu_idLoose;
   Bool_t          mu_idMedium;
   Bool_t          mu_idTight;
   Bool_t          mu_isoFCLoose;
   Bool_t          mu_isoFCTight;
   Bool_t          mu_isoPLVLoose;
   Bool_t          mu_isoPLVTight;
   Bool_t          mu_isoFCLoose_FixedRad;
   Bool_t          mu_isoFCTight_FixedRad;
   Bool_t          mu_isoFixedCutPflowLoose;
   Bool_t          mu_isoFixedCutPflowTight;
   Bool_t          mu2_idLoose;
   Bool_t          mu2_idMedium;
   Bool_t          mu2_idTight;
   Bool_t          mu2_isoFCLoose;
   Bool_t          mu2_isoFCTight;
   Bool_t          mu2_isoPLVLoose;
   Bool_t          mu2_isoPLVTight;
   Bool_t          mu2_isoFCLoose_FixedRad;
   Bool_t          mu2_isoFCTight_FixedRad;
   Bool_t          mu2_isoFixedCutPflowLoose;
   Bool_t          mu2_isoFixedCutPflowTight;
   Bool_t          mu_passIP;
   Bool_t          mu2_passIP;
   Double_t        mu_d0sig;
   Double_t        mu_z0;
   Double_t        mu2_d0sig;
   Double_t        mu2_z0;
   Int_t           mu_truth_type;
   Int_t           mu_truth_origin;
   Int_t           mu_bc;
   Int_t           j_n;
   Double_t        j0_pt;
   Double_t        j0_eta;
   Double_t        j0_phi;
   Double_t        j0_rap;
   Double_t        j0_e;
   Bool_t          j0_JVT;
   Int_t           n_bjets;
   Int_t           n_bjets_60;
   Int_t           n_bjets_70;
   Int_t           n_bjets_77;
   Int_t           n_bjets_85;
   Int_t           n_bjets_continuous;
   Int_t           n_gapjets;
   Double_t        j1_pt;
   Double_t        j1_eta;
   Double_t        j1_phi;
   Double_t        j1_rap;
   Double_t        j1_e;
   Bool_t          j1_JVT;
   Double_t        jj_pt;
   Double_t        jj_eta;
   Double_t        jj_phi;
   Double_t        jj_rap;
   Double_t        jj_e;
   Double_t        jj_m;
   Double_t        jj_deta;
   Double_t        jj_dphi;
   Double_t        jj_dphi_signed;
   Double_t        jj_drap;
   Double_t        jj_dr;
   Double_t        j0met_dphi;
   Double_t        j1met_dphi;
   Double_t        j0gam_dr;
   Double_t        j1gam_dr;
   Double_t        j0lep_dr;
   Double_t        j1lep_dr;
   Double_t        lepgam_dr;
   vector<double>  *j_pt;
   vector<double>  *j_eta;
   vector<double>  *j_phi;
   vector<double>  *j_e;
   Double_t        dPhi_Wy_jj;
   Double_t        dR_Wy_jj;
   Double_t        dR_Wy;
   Double_t        nn_score;

   // List of branches
   TBranch        *b_runnumber;   //!
   TBranch        *b_eventnumber;   //!
   TBranch        *b_dsid;   //!
   TBranch        *b_weight_all;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_prw;   //!
   TBranch        *b_weight_gam;   //!
   TBranch        *b_weight_e;   //!
   TBranch        *b_weight_m;   //!
   TBranch        *b_weight_vertex;   //!
   TBranch        *b_th_syst_weights;   //!
   TBranch        *b_e_trigger_sf;   //!
   TBranch        *b_mu_trigger_sf;   //!
   TBranch        *b_Wy2jEventType;   //!
   TBranch        *b_Wy2jEventFSPartN;   //!
   TBranch        *b_fs_prt_id;   //!
   TBranch        *b_fs_prt_mass;   //!
   TBranch        *b_pass_all;   //!
   TBranch        *b_pass_init;   //!
   TBranch        *b_pass_trigger;   //!
   TBranch        *b_pass_primary_vertex;   //!
   TBranch        *b_n_primary_vertex;   //!
   TBranch        *b_pass_dq;   //!
   TBranch        *b_pass_duplicate;   //!
   TBranch        *b_pass_jetclean;   //!
   TBranch        *b_pass_onelep;   //!
   TBranch        *b_pass_metcut;   //!
   TBranch        *b_pass_onegam;   //!
   TBranch        *b_pass_twojet;   //!
   TBranch        *b_pass_mjj;   //!
   TBranch        *b_pass_second_lepton_veto;   //!
   TBranch        *b_pass_ly_Z_veto;   //!
   TBranch        *b_pass_dphi;   //!
   TBranch        *b_pass_dr;   //!
   TBranch        *b_pass_baseline_e;   //!
   TBranch        *b_pass_baseline_mu;   //!
   TBranch        *b_pass_reco;   //!
   TBranch        *b_isBlind;   //!
   TBranch        *b_pass_onelep_truth;   //!
   TBranch        *b_pass_onegam_truth;   //!
   TBranch        *b_pass_trigger_pt_truth;   //!
   TBranch        *b_pass_ly_Z_veto_truth;   //!
   TBranch        *b_pass_twojet_truth;   //!
   TBranch        *b_pass_dr_truth;   //!
   TBranch        *b_pass_dphi_truth;   //!
   TBranch        *b_pass_baseline_e_truth;   //!
   TBranch        *b_pass_baseline_mu_truth;   //!
   TBranch        *b_pass_truth;   //!
   TBranch        *b_pass_cra_y;   //!
   TBranch        *b_pass_cra_ly;   //!
   TBranch        *b_pass_cra_wy;   //!
   TBranch        *b_pass_crb_y;   //!
   TBranch        *b_pass_crb_ly;   //!
   TBranch        *b_pass_crb_wy;   //!
   TBranch        *b_pass_crc_y;   //!
   TBranch        *b_pass_crc_ly;   //!
   TBranch        *b_pass_crc_wy;   //!
   TBranch        *b_pass_sr_y;   //!
   TBranch        *b_pass_sr_ly;   //!
   TBranch        *b_pass_sr_wy;   //!
   TBranch        *b_pass_cra_y_truth;   //!
   TBranch        *b_pass_cra_ly_truth;   //!
   TBranch        *b_pass_cra_wy_truth;   //!
   TBranch        *b_pass_crb_y_truth;   //!
   TBranch        *b_pass_crb_ly_truth;   //!
   TBranch        *b_pass_crb_wy_truth;   //!
   TBranch        *b_pass_crc_y_truth;   //!
   TBranch        *b_pass_crc_ly_truth;   //!
   TBranch        *b_pass_crc_wy_truth;   //!
   TBranch        *b_pass_sr_y_truth;   //!
   TBranch        *b_pass_sr_ly_truth;   //!
   TBranch        *b_pass_sr_wy_truth;   //!
   TBranch        *b_n_bjets_truth;   //!
   TBranch        *b_j0gam_dr_truth;   //!
   TBranch        *b_j1gam_dr_truth;   //!
   TBranch        *b_j0lep_dr_truth;   //!
   TBranch        *b_j1lep_dr_truth;   //!
   TBranch        *b_j0met_dphi_truth;   //!
   TBranch        *b_j1met_dphi_truth;   //!
   TBranch        *b_lepgam_dr_truth;   //!
   TBranch        *b_is_Wenu;   //!
   TBranch        *b_is_Wmunu;   //!
   TBranch        *b_is_Wenu_truth;   //!
   TBranch        *b_is_Wmunu_truth;   //!
   TBranch        *b_pv_z;   //!
   TBranch        *b_lep_n;   //!
   TBranch        *b_lep_n_truth;   //!
   TBranch        *b_lep_truth_pt;   //!
   TBranch        *b_lep_truth_eta;   //!
   TBranch        *b_lep_pt;   //!
   TBranch        *b_lep_eta;   //!
   TBranch        *b_lep_phi;   //!
   TBranch        *b_lep_truth_phi;   //!
   TBranch        *b_lep_e;   //!
   TBranch        *b_lep_truth_type;   //!
   TBranch        *b_lep_truth_origin;   //!
   TBranch        *b_lep_charge;   //!
   TBranch        *b_lep_truth;   //!
   TBranch        *b_lep2_pt;   //!
   TBranch        *b_lep2_eta;   //!
   TBranch        *b_lep2_phi;   //!
   TBranch        *b_lep2_e;   //!
   TBranch        *b_lep2_charge;   //!
   TBranch        *b_lep2_same_flav;   //!
   TBranch        *b_e_n;   //!
   TBranch        *b_e_pt;   //!
   TBranch        *b_e_eta;   //!
   TBranch        *b_e_phi;   //!
   TBranch        *b_e_e;   //!
   TBranch        *b_e_charge;   //!
   TBranch        *b_e2_pt;   //!
   TBranch        *b_e2_eta;   //!
   TBranch        *b_e2_phi;   //!
   TBranch        *b_e2_e;   //!
   TBranch        *b_e2_charge;   //!
   TBranch        *b_e_truth_pt;   //!
   TBranch        *b_e_truth_eta;   //!
   TBranch        *b_e_truth_phi;   //!
   TBranch        *b_e_truth_e;   //!
   TBranch        *b_e_truth_charge;   //!
   TBranch        *b_e2_truth_pt;   //!
   TBranch        *b_e2_truth_eta;   //!
   TBranch        *b_e2_truth_phi;   //!
   TBranch        *b_e2_truth_e;   //!
   TBranch        *b_e2_truth_charge;   //!
   TBranch        *b_mu_n;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu2_pt;   //!
   TBranch        *b_mu2_eta;   //!
   TBranch        *b_mu2_phi;   //!
   TBranch        *b_mu2_e;   //!
   TBranch        *b_mu2_charge;   //!
   TBranch        *b_mu_truth_pt;   //!
   TBranch        *b_mu_truth_eta;   //!
   TBranch        *b_mu_truth_phi;   //!
   TBranch        *b_mu_truth_e;   //!
   TBranch        *b_mu_truth_charge;   //!
   TBranch        *b_mu2_truth_pt;   //!
   TBranch        *b_mu2_truth_eta;   //!
   TBranch        *b_mu2_truth_phi;   //!
   TBranch        *b_mu2_truth_e;   //!
   TBranch        *b_mu2_truth_charge;   //!
   TBranch        *b_j0_truth_pt;   //!
   TBranch        *b_j0_truth_rap;   //!
   TBranch        *b_j0_truth_eta;   //!
   TBranch        *b_j0_truth_phi;   //!
   TBranch        *b_j0_truth_e;   //!
   TBranch        *b_j1_truth_pt;   //!
   TBranch        *b_j1_truth_rap;   //!
   TBranch        *b_j1_truth_eta;   //!
   TBranch        *b_j1_truth_phi;   //!
   TBranch        *b_j1_truth_e;   //!
   TBranch        *b_jj_truth_m;   //!
   TBranch        *b_jj_truth_drap;   //!
   TBranch        *b_jj_truth_deta;   //!
   TBranch        *b_jj_truth_dphi;   //!
   TBranch        *b_jj_truth_dr;   //!
   TBranch        *b_truth_ngapjets;   //!
   TBranch        *b_e_truth_type;   //!
   TBranch        *b_e_truth_origin;   //!
   TBranch        *b_e_bc;   //!
   TBranch        *b_lep2_truth_type;   //!
   TBranch        *b_lep2_truth_origin;   //!
   TBranch        *b_met_x;   //!
   TBranch        *b_met_y;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_met;   //!
   TBranch        *b_met_eta;   //!
   TBranch        *b_met_truth;   //!
   TBranch        *b_met_x_truth;   //!
   TBranch        *b_met_y_truth;   //!
   TBranch        *b_met_truth_phi;   //!
   TBranch        *b_w_pt;   //!
   TBranch        *b_w_eta;   //!
   TBranch        *b_w_phi;   //!
   TBranch        *b_w_e;   //!
   TBranch        *b_w_m;   //!
   TBranch        *b_w_mt;   //!
   TBranch        *b_w_xi;   //!
   TBranch        *b_w_from_MET_truth_pt;   //!
   TBranch        *b_w_from_MET_truth_eta;   //!
   TBranch        *b_w_from_MET_truth_mT;   //!
   TBranch        *b_w_from_MET_truth_rap;   //!
   TBranch        *b_w_from_MET_truth_E;   //!
   TBranch        *b_w_from_MET_truth_phi;   //!
   TBranch        *b_w_from_MET_truth_xi;   //!
   TBranch        *b_wy_phi;   //!
   TBranch        *b_wy_rap;   //!
   TBranch        *b_wy_m;   //!
   TBranch        *b_wy_pt;   //!
   TBranch        *b_wy_xi;   //!
   TBranch        *b_wy_from_MET_truth_pt;   //!
   TBranch        *b_wy_from_MET_truth_eta;   //!
   TBranch        *b_wy_from_MET_truth_rap;   //!
   TBranch        *b_wy_from_MET_truth_E;   //!
   TBranch        *b_wy_from_MET_truth_phi;   //!
   TBranch        *b_wy_from_MET_truth_xi;   //!
   TBranch        *b_ly_xi;   //!
   TBranch        *b_ly_truth_xi;   //!
   TBranch        *b_gam_n;   //!
   TBranch        *b_gam_pt;   //!
   TBranch        *b_gam_eta;   //!
   TBranch        *b_gam_phi;   //!
   TBranch        *b_gam_e;   //!
   TBranch        *b_gam_xi;   //!
   TBranch        *b_gam_truth_type;   //!
   TBranch        *b_gam_truth_origin;   //!
   TBranch        *b_gam_truth_type_condensed;   //!
   TBranch        *b_gam_truth_pt;   //!
   TBranch        *b_gam_truth_eta;   //!
   TBranch        *b_gam_truth_phi;   //!
   TBranch        *b_gam_truth_e;   //!
   TBranch        *b_gam_truth_xi;   //!
   TBranch        *b_gam_isLoosePrime2;   //!
   TBranch        *b_gam_isLoosePrime3;   //!
   TBranch        *b_gam_idLoose;   //!
   TBranch        *b_gam_isLoosePrime4a;   //!
   TBranch        *b_gam_isLoosePrime5;   //!
   TBranch        *b_gam_topoETcone20;   //!
   TBranch        *b_gam_topoETcone40;   //!
   TBranch        *b_gam_pTcone20;   //!
   TBranch        *b_gam_idTight;   //!
   TBranch        *b_gam_isoTight;   //!
   TBranch        *b_gam_isoLoose;   //!
   TBranch        *b_gam_convtype;   //!
   TBranch        *b_gam_convtrk1nSCThits;   //!
   TBranch        *b_gam_convtrk2nSCThits;   //!
   TBranch        *b_gam_convtrk1nPixhits;   //!
   TBranch        *b_gam_convtrk2nPixhits;   //!
   TBranch        *b_gam_convR;   //!
   TBranch        *b_gam_convz;   //!
   TBranch        *b_gam_truthconvR;   //!
   TBranch        *b_gam_pointz;   //!
   TBranch        *b_gam_pointdz;   //!
   TBranch        *b_gam_sf_ideff;   //!
   TBranch        *b_gam_sf_ideff_unc;   //!
   TBranch        *b_gam_sf_isoeff;   //!
   TBranch        *b_gam_ecalib_ratio;   //!
   TBranch        *b_gam_relereso;   //!
   TBranch        *b_gam2_pt;   //!
   TBranch        *b_gam2_eta;   //!
   TBranch        *b_gam2_phi;   //!
   TBranch        *b_gam2_e;   //!
   TBranch        *b_gam2_xi;   //!
   TBranch        *b_gam2_truth_type;   //!
   TBranch        *b_gam2_truth_origin;   //!
   TBranch        *b_gam2_truth_type_condensed;   //!
   TBranch        *b_gam2_truth_pt;   //!
   TBranch        *b_gam2_truth_eta;   //!
   TBranch        *b_gam2_truth_phi;   //!
   TBranch        *b_gam2_truth_e;   //!
   TBranch        *b_gam2_bc;   //!
   TBranch        *b_gam2_isLoosePrime2;   //!
   TBranch        *b_gam2_isLoosePrime3;   //!
   TBranch        *b_gam2_idLoose;   //!
   TBranch        *b_gam2_isLoosePrime4a;   //!
   TBranch        *b_gam2_isLoosePrime5;   //!
   TBranch        *b_gam2_topoETcone20;   //!
   TBranch        *b_gam2_topoETcone40;   //!
   TBranch        *b_gam2_pTcone20;   //!
   TBranch        *b_gam2_idTight;   //!
   TBranch        *b_gam2_isoTight;   //!
   TBranch        *b_gam2_isoLoose;   //!
   TBranch        *b_gam2_convtype;   //!
   TBranch        *b_gam2_convtrk1nSCThits;   //!
   TBranch        *b_gam2_convtrk2nSCThits;   //!
   TBranch        *b_gam2_convtrk1nPixhits;   //!
   TBranch        *b_gam2_convtrk2nPixhits;   //!
   TBranch        *b_gam2_convR;   //!
   TBranch        *b_gam2_convz;   //!
   TBranch        *b_gam2_truthconvR;   //!
   TBranch        *b_gam2_pointz;   //!
   TBranch        *b_gam2_pointdz;   //!
   TBranch        *b_pass_vy_OR;   //!
   TBranch        *b_e_topoetcone20;   //!
   TBranch        *b_e_pTcone20;   //!
   TBranch        *b_e_pTvarcone30;   //!
   TBranch        *b_e_pTcone20_pt1000;   //!
   TBranch        *b_e_pTcone20_pt500;   //!
   TBranch        *b_e_pTvarcone30_pt1000;   //!
   TBranch        *b_e_pTvarcone30_pt500;   //!
   TBranch        *b_e_neflowisol20;   //!
   TBranch        *b_e_idLoose;   //!
   TBranch        *b_e_idMedium;   //!
   TBranch        *b_e_idTight;   //!
   TBranch        *b_e_isoTight;   //!
   TBranch        *b_e_isoLoose;   //!
   TBranch        *b_e_isoFCTight;   //!
   TBranch        *b_e_isoFCLoose;   //!
   TBranch        *b_e_isoPLVTight;   //!
   TBranch        *b_e_isoPLVLoose;   //!
   TBranch        *b_e2_idLoose;   //!
   TBranch        *b_e2_idMedium;   //!
   TBranch        *b_e2_idTight;   //!
   TBranch        *b_e2_isoTight;   //!
   TBranch        *b_e2_isoLoose;   //!
   TBranch        *b_e2_isoFCTight;   //!
   TBranch        *b_e2_isoFCLoose;   //!
   TBranch        *b_e2_isoPLVTight;   //!
   TBranch        *b_e2_isoPLVLoose;   //!
   TBranch        *b_e_passIP;   //!
   TBranch        *b_e2_passIP;   //!
   TBranch        *b_e_d0sig;   //!
   TBranch        *b_e_z0;   //!
   TBranch        *b_e2_d0sig;   //!
   TBranch        *b_e2_z0;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_pTcone20;   //!
   TBranch        *b_mu_pTvarcone30;   //!
   TBranch        *b_mu_pTcone20_pt1000;   //!
   TBranch        *b_mu_pTcone20_pt500;   //!
   TBranch        *b_mu_pTvarcone30_pt1000;   //!
   TBranch        *b_mu_pTvarcone30_pt500;   //!
   TBranch        *b_mu_neflowisol20;   //!
   TBranch        *b_mu_idLoose;   //!
   TBranch        *b_mu_idMedium;   //!
   TBranch        *b_mu_idTight;   //!
   TBranch        *b_mu_isoFCLoose;   //!
   TBranch        *b_mu_isoFCTight;   //!
   TBranch        *b_mu_isoPLVLoose;   //!
   TBranch        *b_mu_isoPLVTight;   //!
   TBranch        *b_mu_isoFCLoose_FixedRad;   //!
   TBranch        *b_mu_isoFCTight_FixedRad;   //!
   TBranch        *b_mu_isoFixedCutPflowLoose;   //!
   TBranch        *b_mu_isoFixedCutPflowTight;   //!
   TBranch        *b_mu2_idLoose;   //!
   TBranch        *b_mu2_idMedium;   //!
   TBranch        *b_mu2_idTight;   //!
   TBranch        *b_mu2_isoFCLoose;   //!
   TBranch        *b_mu2_isoFCTight;   //!
   TBranch        *b_mu2_isoPLVLoose;   //!
   TBranch        *b_mu2_isoPLVTight;   //!
   TBranch        *b_mu2_isoFCLoose_FixedRad;   //!
   TBranch        *b_mu2_isoFCTight_FixedRad;   //!
   TBranch        *b_mu2_isoFixedCutPflowLoose;   //!
   TBranch        *b_mu2_isoFixedCutPflowTight;   //!
   TBranch        *b_mu_passIP;   //!
   TBranch        *b_mu2_passIP;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_z0;   //!
   TBranch        *b_mu2_d0sig;   //!
   TBranch        *b_mu2_z0;   //!
   TBranch        *b_mu_truth_type;   //!
   TBranch        *b_mu_truth_origin;   //!
   TBranch        *b_mu_bc;   //!
   TBranch        *b_j_n;   //!
   TBranch        *b_j0_pt;   //!
   TBranch        *b_j0_eta;   //!
   TBranch        *b_j0_phi;   //!
   TBranch        *b_j0_rap;   //!
   TBranch        *b_j0_e;   //!
   TBranch        *b_j0_JVT;   //!
   TBranch        *b_n_bjets;   //!
   TBranch        *b_n_bjets_60;   //!
   TBranch        *b_n_bjets_70;   //!
   TBranch        *b_n_bjets_77;   //!
   TBranch        *b_n_bjets_85;   //!
   TBranch        *b_n_bjets_continuous;   //!
   TBranch        *b_n_gapjets;   //!
   TBranch        *b_j1_pt;   //!
   TBranch        *b_j1_eta;   //!
   TBranch        *b_j1_phi;   //!
   TBranch        *b_j1_rap;   //!
   TBranch        *b_j1_e;   //!
   TBranch        *b_j1_JVT;   //!
   TBranch        *b_jj_pt;   //!
   TBranch        *b_jj_eta;   //!
   TBranch        *b_jj_phi;   //!
   TBranch        *b_jj_rap;   //!
   TBranch        *b_jj_e;   //!
   TBranch        *b_jj_m;   //!
   TBranch        *b_jj_deta;   //!
   TBranch        *b_jj_dphi;   //!
   TBranch        *b_jj_dphi_signed;   //!
   TBranch        *b_jj_drap;   //!
   TBranch        *b_jj_dr;   //!
   TBranch        *b_j0met_dphi;   //!
   TBranch        *b_j1met_dphi;   //!
   TBranch        *b_j0gam_dr;   //!
   TBranch        *b_j1gam_dr;   //!
   TBranch        *b_j0lep_dr;   //!
   TBranch        *b_j1lep_dr;   //!
   TBranch        *b_lepgam_dr;   //!
   TBranch        *b_j_pt;   //!
   TBranch        *b_j_eta;   //!
   TBranch        *b_j_phi;   //!
   TBranch        *b_j_e;   //!
   TBranch        *b_dPhi_Wy_jj;   //!
   TBranch        *b_dR_Wy_jj;   //!
   TBranch        *b_dR_Wy;   //!
   TBranch        *b_nn_score;   //!

   vbswy(TTree *tree=0);
   virtual ~vbswy();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   //virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef vbswy_cxx
vbswy::vbswy(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.jmcgowan.vbswy-tree-v06-run1.700016.mc16a.p4097_MxAOD.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.jmcgowan.vbswy-tree-v06-run1.700016.mc16a.p4097_MxAOD.root");
      }
      f->GetObject("vbswy",tree);

   }
   Init(tree);
}

vbswy::~vbswy()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t vbswy::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t vbswy::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void vbswy::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   th_syst_weights = 0;
   fs_prt_id = 0;
   fs_prt_mass = 0;
   j_pt = 0;
   j_eta = 0;
   j_phi = 0;
   j_e = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runnumber", &runnumber, &b_runnumber);
   fChain->SetBranchAddress("eventnumber", &eventnumber, &b_eventnumber);
   fChain->SetBranchAddress("dsid", &dsid, &b_dsid);
   fChain->SetBranchAddress("weight_all", &weight_all, &b_weight_all);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_prw", &weight_prw, &b_weight_prw);
   fChain->SetBranchAddress("weight_gam", &weight_gam, &b_weight_gam);
   fChain->SetBranchAddress("weight_e", &weight_e, &b_weight_e);
   fChain->SetBranchAddress("weight_m", &weight_m, &b_weight_m);
   fChain->SetBranchAddress("weight_vertex", &weight_vertex, &b_weight_vertex);
   fChain->SetBranchAddress("th_syst_weights", &th_syst_weights, &b_th_syst_weights);
   fChain->SetBranchAddress("e_trigger_sf", &e_trigger_sf, &b_e_trigger_sf);
   fChain->SetBranchAddress("mu_trigger_sf", &mu_trigger_sf, &b_mu_trigger_sf);
   fChain->SetBranchAddress("Wy2jEventType", &Wy2jEventType, &b_Wy2jEventType);
   fChain->SetBranchAddress("Wy2jEventFSPartN", &Wy2jEventFSPartN, &b_Wy2jEventFSPartN);
   fChain->SetBranchAddress("fs_prt_id", &fs_prt_id, &b_fs_prt_id);
   fChain->SetBranchAddress("fs_prt_mass", &fs_prt_mass, &b_fs_prt_mass);
   fChain->SetBranchAddress("pass_all", &pass_all, &b_pass_all);
   fChain->SetBranchAddress("pass_init", &pass_init, &b_pass_init);
   fChain->SetBranchAddress("pass_trigger", &pass_trigger, &b_pass_trigger);
   fChain->SetBranchAddress("pass_primary_vertex", &pass_primary_vertex, &b_pass_primary_vertex);
   fChain->SetBranchAddress("n_primary_vertex", &n_primary_vertex, &b_n_primary_vertex);
   fChain->SetBranchAddress("pass_dq", &pass_dq, &b_pass_dq);
   fChain->SetBranchAddress("pass_duplicate", &pass_duplicate, &b_pass_duplicate);
   fChain->SetBranchAddress("pass_jetclean", &pass_jetclean, &b_pass_jetclean);
   fChain->SetBranchAddress("pass_onelep", &pass_onelep, &b_pass_onelep);
   fChain->SetBranchAddress("pass_metcut", &pass_metcut, &b_pass_metcut);
   fChain->SetBranchAddress("pass_onegam", &pass_onegam, &b_pass_onegam);
   fChain->SetBranchAddress("pass_twojet", &pass_twojet, &b_pass_twojet);
   fChain->SetBranchAddress("pass_mjj", &pass_mjj, &b_pass_mjj);
   fChain->SetBranchAddress("pass_second_lepton_veto", &pass_second_lepton_veto, &b_pass_second_lepton_veto);
   fChain->SetBranchAddress("pass_ly_Z_veto", &pass_ly_Z_veto, &b_pass_ly_Z_veto);
   fChain->SetBranchAddress("pass_dphi", &pass_dphi, &b_pass_dphi);
   fChain->SetBranchAddress("pass_dr", &pass_dr, &b_pass_dr);
   fChain->SetBranchAddress("pass_baseline_e", &pass_baseline_e, &b_pass_baseline_e);
   fChain->SetBranchAddress("pass_baseline_mu", &pass_baseline_mu, &b_pass_baseline_mu);
   fChain->SetBranchAddress("pass_reco", &pass_reco, &b_pass_reco);
   fChain->SetBranchAddress("isBlind", &isBlind, &b_isBlind);
   fChain->SetBranchAddress("pass_onelep_truth", &pass_onelep_truth, &b_pass_onelep_truth);
   fChain->SetBranchAddress("pass_onegam_truth", &pass_onegam_truth, &b_pass_onegam_truth);
   fChain->SetBranchAddress("pass_trigger_pt_truth", &pass_trigger_pt_truth, &b_pass_trigger_pt_truth);
   fChain->SetBranchAddress("pass_ly_Z_veto_truth", &pass_ly_Z_veto_truth, &b_pass_ly_Z_veto_truth);
   fChain->SetBranchAddress("pass_twojet_truth", &pass_twojet_truth, &b_pass_twojet_truth);
   fChain->SetBranchAddress("pass_dr_truth", &pass_dr_truth, &b_pass_dr_truth);
   fChain->SetBranchAddress("pass_dphi_truth", &pass_dphi_truth, &b_pass_dphi_truth);
   fChain->SetBranchAddress("pass_baseline_e_truth", &pass_baseline_e_truth, &b_pass_baseline_e_truth);
   fChain->SetBranchAddress("pass_baseline_mu_truth", &pass_baseline_mu_truth, &b_pass_baseline_mu_truth);
   fChain->SetBranchAddress("pass_truth", &pass_truth, &b_pass_truth);
   fChain->SetBranchAddress("pass_cra_y", &pass_cra_y, &b_pass_cra_y);
   fChain->SetBranchAddress("pass_cra_ly", &pass_cra_ly, &b_pass_cra_ly);
   fChain->SetBranchAddress("pass_cra_wy", &pass_cra_wy, &b_pass_cra_wy);
   fChain->SetBranchAddress("pass_crb_y", &pass_crb_y, &b_pass_crb_y);
   fChain->SetBranchAddress("pass_crb_ly", &pass_crb_ly, &b_pass_crb_ly);
   fChain->SetBranchAddress("pass_crb_wy", &pass_crb_wy, &b_pass_crb_wy);
   fChain->SetBranchAddress("pass_crc_y", &pass_crc_y, &b_pass_crc_y);
   fChain->SetBranchAddress("pass_crc_ly", &pass_crc_ly, &b_pass_crc_ly);
   fChain->SetBranchAddress("pass_crc_wy", &pass_crc_wy, &b_pass_crc_wy);
   fChain->SetBranchAddress("pass_sr_y", &pass_sr_y, &b_pass_sr_y);
   fChain->SetBranchAddress("pass_sr_ly", &pass_sr_ly, &b_pass_sr_ly);
   fChain->SetBranchAddress("pass_sr_wy", &pass_sr_wy, &b_pass_sr_wy);
   fChain->SetBranchAddress("pass_cra_y_truth", &pass_cra_y_truth, &b_pass_cra_y_truth);
   fChain->SetBranchAddress("pass_cra_ly_truth", &pass_cra_ly_truth, &b_pass_cra_ly_truth);
   fChain->SetBranchAddress("pass_cra_wy_truth", &pass_cra_wy_truth, &b_pass_cra_wy_truth);
   fChain->SetBranchAddress("pass_crb_y_truth", &pass_crb_y_truth, &b_pass_crb_y_truth);
   fChain->SetBranchAddress("pass_crb_ly_truth", &pass_crb_ly_truth, &b_pass_crb_ly_truth);
   fChain->SetBranchAddress("pass_crb_wy_truth", &pass_crb_wy_truth, &b_pass_crb_wy_truth);
   fChain->SetBranchAddress("pass_crc_y_truth", &pass_crc_y_truth, &b_pass_crc_y_truth);
   fChain->SetBranchAddress("pass_crc_ly_truth", &pass_crc_ly_truth, &b_pass_crc_ly_truth);
   fChain->SetBranchAddress("pass_crc_wy_truth", &pass_crc_wy_truth, &b_pass_crc_wy_truth);
   fChain->SetBranchAddress("pass_sr_y_truth", &pass_sr_y_truth, &b_pass_sr_y_truth);
   fChain->SetBranchAddress("pass_sr_ly_truth", &pass_sr_ly_truth, &b_pass_sr_ly_truth);
   fChain->SetBranchAddress("pass_sr_wy_truth", &pass_sr_wy_truth, &b_pass_sr_wy_truth);
   fChain->SetBranchAddress("n_bjets_truth", &n_bjets_truth, &b_n_bjets_truth);
   fChain->SetBranchAddress("j0gam_dr_truth", &j0gam_dr_truth, &b_j0gam_dr_truth);
   fChain->SetBranchAddress("j1gam_dr_truth", &j1gam_dr_truth, &b_j1gam_dr_truth);
   fChain->SetBranchAddress("j0lep_dr_truth", &j0lep_dr_truth, &b_j0lep_dr_truth);
   fChain->SetBranchAddress("j1lep_dr_truth", &j1lep_dr_truth, &b_j1lep_dr_truth);
   fChain->SetBranchAddress("j0met_dphi_truth", &j0met_dphi_truth, &b_j0met_dphi_truth);
   fChain->SetBranchAddress("j1met_dphi_truth", &j1met_dphi_truth, &b_j1met_dphi_truth);
   fChain->SetBranchAddress("lepgam_dr_truth", &lepgam_dr_truth, &b_lepgam_dr_truth);
   fChain->SetBranchAddress("is_Wenu", &is_Wenu, &b_is_Wenu);
   fChain->SetBranchAddress("is_Wmunu", &is_Wmunu, &b_is_Wmunu);
   fChain->SetBranchAddress("is_Wenu_truth", &is_Wenu_truth, &b_is_Wenu_truth);
   fChain->SetBranchAddress("is_Wmunu_truth", &is_Wmunu_truth, &b_is_Wmunu_truth);
   fChain->SetBranchAddress("pv_z", &pv_z, &b_pv_z);
   fChain->SetBranchAddress("lep_n", &lep_n, &b_lep_n);
   fChain->SetBranchAddress("lep_n_truth", &lep_n_truth, &b_lep_n_truth);
   fChain->SetBranchAddress("lep_truth_pt", &lep_truth_pt, &b_lep_truth_pt);
   fChain->SetBranchAddress("lep_truth_eta", &lep_truth_eta, &b_lep_truth_eta);
   fChain->SetBranchAddress("lep_pt", &lep_pt, &b_lep_pt);
   fChain->SetBranchAddress("lep_eta", &lep_eta, &b_lep_eta);
   fChain->SetBranchAddress("lep_phi", &lep_phi, &b_lep_phi);
   fChain->SetBranchAddress("lep_truth_phi", &lep_truth_phi, &b_lep_truth_phi);
   fChain->SetBranchAddress("lep_e", &lep_e, &b_lep_e);
   fChain->SetBranchAddress("lep_truth_type", &lep_truth_type, &b_lep_truth_type);
   fChain->SetBranchAddress("lep_truth_origin", &lep_truth_origin, &b_lep_truth_origin);
   fChain->SetBranchAddress("lep_charge", &lep_charge, &b_lep_charge);
   fChain->SetBranchAddress("lep_truth", &lep_truth, &b_lep_truth);
   fChain->SetBranchAddress("lep2_pt", &lep2_pt, &b_lep2_pt);
   fChain->SetBranchAddress("lep2_eta", &lep2_eta, &b_lep2_eta);
   fChain->SetBranchAddress("lep2_phi", &lep2_phi, &b_lep2_phi);
   fChain->SetBranchAddress("lep2_e", &lep2_e, &b_lep2_e);
   fChain->SetBranchAddress("lep2_charge", &lep2_charge, &b_lep2_charge);
   fChain->SetBranchAddress("lep2_same_flav", &lep2_same_flav, &b_lep2_same_flav);
   fChain->SetBranchAddress("e_n", &e_n, &b_e_n);
   fChain->SetBranchAddress("e_pt", &e_pt, &b_e_pt);
   fChain->SetBranchAddress("e_eta", &e_eta, &b_e_eta);
   fChain->SetBranchAddress("e_phi", &e_phi, &b_e_phi);
   fChain->SetBranchAddress("e_e", &e_e, &b_e_e);
   fChain->SetBranchAddress("e_charge", &e_charge, &b_e_charge);
   fChain->SetBranchAddress("e2_pt", &e2_pt, &b_e2_pt);
   fChain->SetBranchAddress("e2_eta", &e2_eta, &b_e2_eta);
   fChain->SetBranchAddress("e2_phi", &e2_phi, &b_e2_phi);
   fChain->SetBranchAddress("e2_e", &e2_e, &b_e2_e);
   fChain->SetBranchAddress("e2_charge", &e2_charge, &b_e2_charge);
   fChain->SetBranchAddress("e_truth_pt", &e_truth_pt, &b_e_truth_pt);
   fChain->SetBranchAddress("e_truth_eta", &e_truth_eta, &b_e_truth_eta);
   fChain->SetBranchAddress("e_truth_phi", &e_truth_phi, &b_e_truth_phi);
   fChain->SetBranchAddress("e_truth_e", &e_truth_e, &b_e_truth_e);
   fChain->SetBranchAddress("e_truth_charge", &e_truth_charge, &b_e_truth_charge);
   fChain->SetBranchAddress("e2_truth_pt", &e2_truth_pt, &b_e2_truth_pt);
   fChain->SetBranchAddress("e2_truth_eta", &e2_truth_eta, &b_e2_truth_eta);
   fChain->SetBranchAddress("e2_truth_phi", &e2_truth_phi, &b_e2_truth_phi);
   fChain->SetBranchAddress("e2_truth_e", &e2_truth_e, &b_e2_truth_e);
   fChain->SetBranchAddress("e2_truth_charge", &e2_truth_charge, &b_e2_truth_charge);
   fChain->SetBranchAddress("mu_n", &mu_n, &b_mu_n);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu2_pt", &mu2_pt, &b_mu2_pt);
   fChain->SetBranchAddress("mu2_eta", &mu2_eta, &b_mu2_eta);
   fChain->SetBranchAddress("mu2_phi", &mu2_phi, &b_mu2_phi);
   fChain->SetBranchAddress("mu2_e", &mu2_e, &b_mu2_e);
   fChain->SetBranchAddress("mu2_charge", &mu2_charge, &b_mu2_charge);
   fChain->SetBranchAddress("mu_truth_pt", &mu_truth_pt, &b_mu_truth_pt);
   fChain->SetBranchAddress("mu_truth_eta", &mu_truth_eta, &b_mu_truth_eta);
   fChain->SetBranchAddress("mu_truth_phi", &mu_truth_phi, &b_mu_truth_phi);
   fChain->SetBranchAddress("mu_truth_e", &mu_truth_e, &b_mu_truth_e);
   fChain->SetBranchAddress("mu_truth_charge", &mu_truth_charge, &b_mu_truth_charge);
   fChain->SetBranchAddress("mu2_truth_pt", &mu2_truth_pt, &b_mu2_truth_pt);
   fChain->SetBranchAddress("mu2_truth_eta", &mu2_truth_eta, &b_mu2_truth_eta);
   fChain->SetBranchAddress("mu2_truth_phi", &mu2_truth_phi, &b_mu2_truth_phi);
   fChain->SetBranchAddress("mu2_truth_e", &mu2_truth_e, &b_mu2_truth_e);
   fChain->SetBranchAddress("mu2_truth_charge", &mu2_truth_charge, &b_mu2_truth_charge);
   fChain->SetBranchAddress("j0_truth_pt", &j0_truth_pt, &b_j0_truth_pt);
   fChain->SetBranchAddress("j0_truth_rap", &j0_truth_rap, &b_j0_truth_rap);
   fChain->SetBranchAddress("j0_truth_eta", &j0_truth_eta, &b_j0_truth_eta);
   fChain->SetBranchAddress("j0_truth_phi", &j0_truth_phi, &b_j0_truth_phi);
   fChain->SetBranchAddress("j0_truth_e", &j0_truth_e, &b_j0_truth_e);
   fChain->SetBranchAddress("j1_truth_pt", &j1_truth_pt, &b_j1_truth_pt);
   fChain->SetBranchAddress("j1_truth_rap", &j1_truth_rap, &b_j1_truth_rap);
   fChain->SetBranchAddress("j1_truth_eta", &j1_truth_eta, &b_j1_truth_eta);
   fChain->SetBranchAddress("j1_truth_phi", &j1_truth_phi, &b_j1_truth_phi);
   fChain->SetBranchAddress("j1_truth_e", &j1_truth_e, &b_j1_truth_e);
   fChain->SetBranchAddress("jj_truth_m", &jj_truth_m, &b_jj_truth_m);
   fChain->SetBranchAddress("jj_truth_drap", &jj_truth_drap, &b_jj_truth_drap);
   fChain->SetBranchAddress("jj_truth_deta", &jj_truth_deta, &b_jj_truth_deta);
   fChain->SetBranchAddress("jj_truth_dphi", &jj_truth_dphi, &b_jj_truth_dphi);
   fChain->SetBranchAddress("jj_truth_dr", &jj_truth_dr, &b_jj_truth_dr);
   fChain->SetBranchAddress("truth_ngapjets", &truth_ngapjets, &b_truth_ngapjets);
   fChain->SetBranchAddress("e_truth_type", &e_truth_type, &b_e_truth_type);
   fChain->SetBranchAddress("e_truth_origin", &e_truth_origin, &b_e_truth_origin);
   fChain->SetBranchAddress("e_bc", &e_bc, &b_e_bc);
   fChain->SetBranchAddress("lep2_truth_type", &lep2_truth_type, &b_lep2_truth_type);
   fChain->SetBranchAddress("lep2_truth_origin", &lep2_truth_origin, &b_lep2_truth_origin);
   fChain->SetBranchAddress("met_x", &met_x, &b_met_x);
   fChain->SetBranchAddress("met_y", &met_y, &b_met_y);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("met", &met, &b_met);
   fChain->SetBranchAddress("met_eta", &met_eta, &b_met_eta);
   fChain->SetBranchAddress("met_truth", &met_truth, &b_met_truth);
   fChain->SetBranchAddress("met_x_truth", &met_x_truth, &b_met_x_truth);
   fChain->SetBranchAddress("met_y_truth", &met_y_truth, &b_met_y_truth);
   fChain->SetBranchAddress("met_truth_phi", &met_truth_phi, &b_met_truth_phi);
   fChain->SetBranchAddress("w_pt", &w_pt, &b_w_pt);
   fChain->SetBranchAddress("w_eta", &w_eta, &b_w_eta);
   fChain->SetBranchAddress("w_phi", &w_phi, &b_w_phi);
   fChain->SetBranchAddress("w_e", &w_e, &b_w_e);
   fChain->SetBranchAddress("w_m", &w_m, &b_w_m);
   fChain->SetBranchAddress("w_mt", &w_mt, &b_w_mt);
   fChain->SetBranchAddress("w_xi", &w_xi, &b_w_xi);
   fChain->SetBranchAddress("w_from_MET_truth_pt", &w_from_MET_truth_pt, &b_w_from_MET_truth_pt);
   fChain->SetBranchAddress("w_from_MET_truth_eta", &w_from_MET_truth_eta, &b_w_from_MET_truth_eta);
   fChain->SetBranchAddress("w_from_MET_truth_mT", &w_from_MET_truth_mT, &b_w_from_MET_truth_mT);
   fChain->SetBranchAddress("w_from_MET_truth_rap", &w_from_MET_truth_rap, &b_w_from_MET_truth_rap);
   fChain->SetBranchAddress("w_from_MET_truth_E", &w_from_MET_truth_E, &b_w_from_MET_truth_E);
   fChain->SetBranchAddress("w_from_MET_truth_phi", &w_from_MET_truth_phi, &b_w_from_MET_truth_phi);
   fChain->SetBranchAddress("w_from_MET_truth_xi", &w_from_MET_truth_xi, &b_w_from_MET_truth_xi);
   fChain->SetBranchAddress("wy_phi", &wy_phi, &b_wy_phi);
   fChain->SetBranchAddress("wy_rap", &wy_rap, &b_wy_rap);
   fChain->SetBranchAddress("wy_m", &wy_m, &b_wy_m);
   fChain->SetBranchAddress("wy_pt", &wy_pt, &b_wy_pt);
   fChain->SetBranchAddress("wy_xi", &wy_xi, &b_wy_xi);
   fChain->SetBranchAddress("wy_from_MET_truth_pt", &wy_from_MET_truth_pt, &b_wy_from_MET_truth_pt);
   fChain->SetBranchAddress("wy_from_MET_truth_eta", &wy_from_MET_truth_eta, &b_wy_from_MET_truth_eta);
   fChain->SetBranchAddress("wy_from_MET_truth_rap", &wy_from_MET_truth_rap, &b_wy_from_MET_truth_rap);
   fChain->SetBranchAddress("wy_from_MET_truth_E", &wy_from_MET_truth_E, &b_wy_from_MET_truth_E);
   fChain->SetBranchAddress("wy_from_MET_truth_phi", &wy_from_MET_truth_phi, &b_wy_from_MET_truth_phi);
   fChain->SetBranchAddress("wy_from_MET_truth_xi", &wy_from_MET_truth_xi, &b_wy_from_MET_truth_xi);
   fChain->SetBranchAddress("ly_xi", &ly_xi, &b_ly_xi);
   fChain->SetBranchAddress("ly_truth_xi", &ly_truth_xi, &b_ly_truth_xi);
   fChain->SetBranchAddress("gam_n", &gam_n, &b_gam_n);
   fChain->SetBranchAddress("gam_pt", &gam_pt, &b_gam_pt);
   fChain->SetBranchAddress("gam_eta", &gam_eta, &b_gam_eta);
   fChain->SetBranchAddress("gam_phi", &gam_phi, &b_gam_phi);
   fChain->SetBranchAddress("gam_e", &gam_e, &b_gam_e);
   fChain->SetBranchAddress("gam_xi", &gam_xi, &b_gam_xi);
   fChain->SetBranchAddress("gam_truth_type", &gam_truth_type, &b_gam_truth_type);
   fChain->SetBranchAddress("gam_truth_origin", &gam_truth_origin, &b_gam_truth_origin);
   fChain->SetBranchAddress("gam_truth_type_condensed", &gam_truth_type_condensed, &b_gam_truth_type_condensed);
   fChain->SetBranchAddress("gam_truth_pt", &gam_truth_pt, &b_gam_truth_pt);
   fChain->SetBranchAddress("gam_truth_eta", &gam_truth_eta, &b_gam_truth_eta);
   fChain->SetBranchAddress("gam_truth_phi", &gam_truth_phi, &b_gam_truth_phi);
   fChain->SetBranchAddress("gam_truth_e", &gam_truth_e, &b_gam_truth_e);
   fChain->SetBranchAddress("gam_truth_xi", &gam_truth_xi, &b_gam_truth_xi);
   fChain->SetBranchAddress("gam_isLoosePrime2", &gam_isLoosePrime2, &b_gam_isLoosePrime2);
   fChain->SetBranchAddress("gam_isLoosePrime3", &gam_isLoosePrime3, &b_gam_isLoosePrime3);
   fChain->SetBranchAddress("gam_idLoose", &gam_idLoose, &b_gam_idLoose);
   fChain->SetBranchAddress("gam_isLoosePrime4a", &gam_isLoosePrime4a, &b_gam_isLoosePrime4a);
   fChain->SetBranchAddress("gam_isLoosePrime5", &gam_isLoosePrime5, &b_gam_isLoosePrime5);
   fChain->SetBranchAddress("gam_topoETcone20", &gam_topoETcone20, &b_gam_topoETcone20);
   fChain->SetBranchAddress("gam_topoETcone40", &gam_topoETcone40, &b_gam_topoETcone40);
   fChain->SetBranchAddress("gam_pTcone20", &gam_pTcone20, &b_gam_pTcone20);
   fChain->SetBranchAddress("gam_idTight", &gam_idTight, &b_gam_idTight);
   fChain->SetBranchAddress("gam_isoTight", &gam_isoTight, &b_gam_isoTight);
   fChain->SetBranchAddress("gam_isoLoose", &gam_isoLoose, &b_gam_isoLoose);
   fChain->SetBranchAddress("gam_convtype", &gam_convtype, &b_gam_convtype);
   fChain->SetBranchAddress("gam_convtrk1nSCThits", &gam_convtrk1nSCThits, &b_gam_convtrk1nSCThits);
   fChain->SetBranchAddress("gam_convtrk2nSCThits", &gam_convtrk2nSCThits, &b_gam_convtrk2nSCThits);
   fChain->SetBranchAddress("gam_convtrk1nPixhits", &gam_convtrk1nPixhits, &b_gam_convtrk1nPixhits);
   fChain->SetBranchAddress("gam_convtrk2nPixhits", &gam_convtrk2nPixhits, &b_gam_convtrk2nPixhits);
   fChain->SetBranchAddress("gam_convR", &gam_convR, &b_gam_convR);
   fChain->SetBranchAddress("gam_convz", &gam_convz, &b_gam_convz);
   fChain->SetBranchAddress("gam_truthconvR", &gam_truthconvR, &b_gam_truthconvR);
   fChain->SetBranchAddress("gam_pointz", &gam_pointz, &b_gam_pointz);
   fChain->SetBranchAddress("gam_pointdz", &gam_pointdz, &b_gam_pointdz);
   fChain->SetBranchAddress("gam_sf_ideff", &gam_sf_ideff, &b_gam_sf_ideff);
   fChain->SetBranchAddress("gam_sf_ideff_unc", &gam_sf_ideff_unc, &b_gam_sf_ideff_unc);
   fChain->SetBranchAddress("gam_sf_isoeff", &gam_sf_isoeff, &b_gam_sf_isoeff);
   fChain->SetBranchAddress("gam_ecalib_ratio", &gam_ecalib_ratio, &b_gam_ecalib_ratio);
   fChain->SetBranchAddress("gam_relereso", &gam_relereso, &b_gam_relereso);
   fChain->SetBranchAddress("gam2_pt", &gam2_pt, &b_gam2_pt);
   fChain->SetBranchAddress("gam2_eta", &gam2_eta, &b_gam2_eta);
   fChain->SetBranchAddress("gam2_phi", &gam2_phi, &b_gam2_phi);
   fChain->SetBranchAddress("gam2_e", &gam2_e, &b_gam2_e);
   fChain->SetBranchAddress("gam2_xi", &gam2_xi, &b_gam2_xi);
   fChain->SetBranchAddress("gam2_truth_type", &gam2_truth_type, &b_gam2_truth_type);
   fChain->SetBranchAddress("gam2_truth_origin", &gam2_truth_origin, &b_gam2_truth_origin);
   fChain->SetBranchAddress("gam2_truth_type_condensed", &gam2_truth_type_condensed, &b_gam2_truth_type_condensed);
   fChain->SetBranchAddress("gam2_truth_pt", &gam2_truth_pt, &b_gam2_truth_pt);
   fChain->SetBranchAddress("gam2_truth_eta", &gam2_truth_eta, &b_gam2_truth_eta);
   fChain->SetBranchAddress("gam2_truth_phi", &gam2_truth_phi, &b_gam2_truth_phi);
   fChain->SetBranchAddress("gam2_truth_e", &gam2_truth_e, &b_gam2_truth_e);
   fChain->SetBranchAddress("gam2_bc", &gam2_bc, &b_gam2_bc);
   fChain->SetBranchAddress("gam2_isLoosePrime2", &gam2_isLoosePrime2, &b_gam2_isLoosePrime2);
   fChain->SetBranchAddress("gam2_isLoosePrime3", &gam2_isLoosePrime3, &b_gam2_isLoosePrime3);
   fChain->SetBranchAddress("gam2_idLoose", &gam2_idLoose, &b_gam2_idLoose);
   fChain->SetBranchAddress("gam2_isLoosePrime4a", &gam2_isLoosePrime4a, &b_gam2_isLoosePrime4a);
   fChain->SetBranchAddress("gam2_isLoosePrime5", &gam2_isLoosePrime5, &b_gam2_isLoosePrime5);
   fChain->SetBranchAddress("gam2_topoETcone20", &gam2_topoETcone20, &b_gam2_topoETcone20);
   fChain->SetBranchAddress("gam2_topoETcone40", &gam2_topoETcone40, &b_gam2_topoETcone40);
   fChain->SetBranchAddress("gam2_pTcone20", &gam2_pTcone20, &b_gam2_pTcone20);
   fChain->SetBranchAddress("gam2_idTight", &gam2_idTight, &b_gam2_idTight);
   fChain->SetBranchAddress("gam2_isoTight", &gam2_isoTight, &b_gam2_isoTight);
   fChain->SetBranchAddress("gam2_isoLoose", &gam2_isoLoose, &b_gam2_isoLoose);
   fChain->SetBranchAddress("gam2_convtype", &gam2_convtype, &b_gam2_convtype);
   fChain->SetBranchAddress("gam2_convtrk1nSCThits", &gam2_convtrk1nSCThits, &b_gam2_convtrk1nSCThits);
   fChain->SetBranchAddress("gam2_convtrk2nSCThits", &gam2_convtrk2nSCThits, &b_gam2_convtrk2nSCThits);
   fChain->SetBranchAddress("gam2_convtrk1nPixhits", &gam2_convtrk1nPixhits, &b_gam2_convtrk1nPixhits);
   fChain->SetBranchAddress("gam2_convtrk2nPixhits", &gam2_convtrk2nPixhits, &b_gam2_convtrk2nPixhits);
   fChain->SetBranchAddress("gam2_convR", &gam2_convR, &b_gam2_convR);
   fChain->SetBranchAddress("gam2_convz", &gam2_convz, &b_gam2_convz);
   fChain->SetBranchAddress("gam2_truthconvR", &gam2_truthconvR, &b_gam2_truthconvR);
   fChain->SetBranchAddress("gam2_pointz", &gam2_pointz, &b_gam2_pointz);
   fChain->SetBranchAddress("gam2_pointdz", &gam2_pointdz, &b_gam2_pointdz);
   fChain->SetBranchAddress("pass_vy_OR", &pass_vy_OR, &b_pass_vy_OR);
   fChain->SetBranchAddress("e_topoetcone20", &e_topoetcone20, &b_e_topoetcone20);
   fChain->SetBranchAddress("e_pTcone20", &e_pTcone20, &b_e_pTcone20);
   fChain->SetBranchAddress("e_pTvarcone30", &e_pTvarcone30, &b_e_pTvarcone30);
   fChain->SetBranchAddress("e_pTcone20_pt1000", &e_pTcone20_pt1000, &b_e_pTcone20_pt1000);
   fChain->SetBranchAddress("e_pTcone20_pt500", &e_pTcone20_pt500, &b_e_pTcone20_pt500);
   fChain->SetBranchAddress("e_pTvarcone30_pt1000", &e_pTvarcone30_pt1000, &b_e_pTvarcone30_pt1000);
   fChain->SetBranchAddress("e_pTvarcone30_pt500", &e_pTvarcone30_pt500, &b_e_pTvarcone30_pt500);
   fChain->SetBranchAddress("e_neflowisol20", &e_neflowisol20, &b_e_neflowisol20);
   fChain->SetBranchAddress("e_idLoose", &e_idLoose, &b_e_idLoose);
   fChain->SetBranchAddress("e_idMedium", &e_idMedium, &b_e_idMedium);
   fChain->SetBranchAddress("e_idTight", &e_idTight, &b_e_idTight);
   fChain->SetBranchAddress("e_isoTight", &e_isoTight, &b_e_isoTight);
   fChain->SetBranchAddress("e_isoLoose", &e_isoLoose, &b_e_isoLoose);
   fChain->SetBranchAddress("e_isoFCTight", &e_isoFCTight, &b_e_isoFCTight);
   fChain->SetBranchAddress("e_isoFCLoose", &e_isoFCLoose, &b_e_isoFCLoose);
   fChain->SetBranchAddress("e_isoPLVTight", &e_isoPLVTight, &b_e_isoPLVTight);
   fChain->SetBranchAddress("e_isoPLVLoose", &e_isoPLVLoose, &b_e_isoPLVLoose);
   fChain->SetBranchAddress("e2_idLoose", &e2_idLoose, &b_e2_idLoose);
   fChain->SetBranchAddress("e2_idMedium", &e2_idMedium, &b_e2_idMedium);
   fChain->SetBranchAddress("e2_idTight", &e2_idTight, &b_e2_idTight);
   fChain->SetBranchAddress("e2_isoTight", &e2_isoTight, &b_e2_isoTight);
   fChain->SetBranchAddress("e2_isoLoose", &e2_isoLoose, &b_e2_isoLoose);
   fChain->SetBranchAddress("e2_isoFCTight", &e2_isoFCTight, &b_e2_isoFCTight);
   fChain->SetBranchAddress("e2_isoFCLoose", &e2_isoFCLoose, &b_e2_isoFCLoose);
   fChain->SetBranchAddress("e2_isoPLVTight", &e2_isoPLVTight, &b_e2_isoPLVTight);
   fChain->SetBranchAddress("e2_isoPLVLoose", &e2_isoPLVLoose, &b_e2_isoPLVLoose);
   fChain->SetBranchAddress("e_passIP", &e_passIP, &b_e_passIP);
   fChain->SetBranchAddress("e2_passIP", &e2_passIP, &b_e2_passIP);
   fChain->SetBranchAddress("e_d0sig", &e_d0sig, &b_e_d0sig);
   fChain->SetBranchAddress("e_z0", &e_z0, &b_e_z0);
   fChain->SetBranchAddress("e2_d0sig", &e2_d0sig, &b_e2_d0sig);
   fChain->SetBranchAddress("e2_z0", &e2_z0, &b_e2_z0);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_pTcone20", &mu_pTcone20, &b_mu_pTcone20);
   fChain->SetBranchAddress("mu_pTvarcone30", &mu_pTvarcone30, &b_mu_pTvarcone30);
   fChain->SetBranchAddress("mu_pTcone20_pt1000", &mu_pTcone20_pt1000, &b_mu_pTcone20_pt1000);
   fChain->SetBranchAddress("mu_pTcone20_pt500", &mu_pTcone20_pt500, &b_mu_pTcone20_pt500);
   fChain->SetBranchAddress("mu_pTvarcone30_pt1000", &mu_pTvarcone30_pt1000, &b_mu_pTvarcone30_pt1000);
   fChain->SetBranchAddress("mu_pTvarcone30_pt500", &mu_pTvarcone30_pt500, &b_mu_pTvarcone30_pt500);
   fChain->SetBranchAddress("mu_neflowisol20", &mu_neflowisol20, &b_mu_neflowisol20);
   fChain->SetBranchAddress("mu_idLoose", &mu_idLoose, &b_mu_idLoose);
   fChain->SetBranchAddress("mu_idMedium", &mu_idMedium, &b_mu_idMedium);
   fChain->SetBranchAddress("mu_idTight", &mu_idTight, &b_mu_idTight);
   fChain->SetBranchAddress("mu_isoFCLoose", &mu_isoFCLoose, &b_mu_isoFCLoose);
   fChain->SetBranchAddress("mu_isoFCTight", &mu_isoFCTight, &b_mu_isoFCTight);
   fChain->SetBranchAddress("mu_isoPLVLoose", &mu_isoPLVLoose, &b_mu_isoPLVLoose);
   fChain->SetBranchAddress("mu_isoPLVTight", &mu_isoPLVTight, &b_mu_isoPLVTight);
   fChain->SetBranchAddress("mu_isoFCLoose_FixedRad", &mu_isoFCLoose_FixedRad, &b_mu_isoFCLoose_FixedRad);
   fChain->SetBranchAddress("mu_isoFCTight_FixedRad", &mu_isoFCTight_FixedRad, &b_mu_isoFCTight_FixedRad);
   fChain->SetBranchAddress("mu_isoFixedCutPflowLoose", &mu_isoFixedCutPflowLoose, &b_mu_isoFixedCutPflowLoose);
   fChain->SetBranchAddress("mu_isoFixedCutPflowTight", &mu_isoFixedCutPflowTight, &b_mu_isoFixedCutPflowTight);
   fChain->SetBranchAddress("mu2_idLoose", &mu2_idLoose, &b_mu2_idLoose);
   fChain->SetBranchAddress("mu2_idMedium", &mu2_idMedium, &b_mu2_idMedium);
   fChain->SetBranchAddress("mu2_idTight", &mu2_idTight, &b_mu2_idTight);
   fChain->SetBranchAddress("mu2_isoFCLoose", &mu2_isoFCLoose, &b_mu2_isoFCLoose);
   fChain->SetBranchAddress("mu2_isoFCTight", &mu2_isoFCTight, &b_mu2_isoFCTight);
   fChain->SetBranchAddress("mu2_isoPLVLoose", &mu2_isoPLVLoose, &b_mu2_isoPLVLoose);
   fChain->SetBranchAddress("mu2_isoPLVTight", &mu2_isoPLVTight, &b_mu2_isoPLVTight);
   fChain->SetBranchAddress("mu2_isoFCLoose_FixedRad", &mu2_isoFCLoose_FixedRad, &b_mu2_isoFCLoose_FixedRad);
   fChain->SetBranchAddress("mu2_isoFCTight_FixedRad", &mu2_isoFCTight_FixedRad, &b_mu2_isoFCTight_FixedRad);
   fChain->SetBranchAddress("mu2_isoFixedCutPflowLoose", &mu2_isoFixedCutPflowLoose, &b_mu2_isoFixedCutPflowLoose);
   fChain->SetBranchAddress("mu2_isoFixedCutPflowTight", &mu2_isoFixedCutPflowTight, &b_mu2_isoFixedCutPflowTight);
   fChain->SetBranchAddress("mu_passIP", &mu_passIP, &b_mu_passIP);
   fChain->SetBranchAddress("mu2_passIP", &mu2_passIP, &b_mu2_passIP);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_z0", &mu_z0, &b_mu_z0);
   fChain->SetBranchAddress("mu2_d0sig", &mu2_d0sig, &b_mu2_d0sig);
   fChain->SetBranchAddress("mu2_z0", &mu2_z0, &b_mu2_z0);
   fChain->SetBranchAddress("mu_truth_type", &mu_truth_type, &b_mu_truth_type);
   fChain->SetBranchAddress("mu_truth_origin", &mu_truth_origin, &b_mu_truth_origin);
   fChain->SetBranchAddress("mu_bc", &mu_bc, &b_mu_bc);
   fChain->SetBranchAddress("j_n", &j_n, &b_j_n);
   fChain->SetBranchAddress("j0_pt", &j0_pt, &b_j0_pt);
   fChain->SetBranchAddress("j0_eta", &j0_eta, &b_j0_eta);
   fChain->SetBranchAddress("j0_phi", &j0_phi, &b_j0_phi);
   fChain->SetBranchAddress("j0_rap", &j0_rap, &b_j0_rap);
   fChain->SetBranchAddress("j0_e", &j0_e, &b_j0_e);
   fChain->SetBranchAddress("j0_JVT", &j0_JVT, &b_j0_JVT);
   fChain->SetBranchAddress("n_bjets", &n_bjets, &b_n_bjets);
   fChain->SetBranchAddress("n_bjets_60", &n_bjets_60, &b_n_bjets_60);
   fChain->SetBranchAddress("n_bjets_70", &n_bjets_70, &b_n_bjets_70);
   fChain->SetBranchAddress("n_bjets_77", &n_bjets_77, &b_n_bjets_77);
   fChain->SetBranchAddress("n_bjets_85", &n_bjets_85, &b_n_bjets_85);
   fChain->SetBranchAddress("n_bjets_continuous", &n_bjets_continuous, &b_n_bjets_continuous);
   fChain->SetBranchAddress("n_gapjets", &n_gapjets, &b_n_gapjets);
   fChain->SetBranchAddress("j1_pt", &j1_pt, &b_j1_pt);
   fChain->SetBranchAddress("j1_eta", &j1_eta, &b_j1_eta);
   fChain->SetBranchAddress("j1_phi", &j1_phi, &b_j1_phi);
   fChain->SetBranchAddress("j1_rap", &j1_rap, &b_j1_rap);
   fChain->SetBranchAddress("j1_e", &j1_e, &b_j1_e);
   fChain->SetBranchAddress("j1_JVT", &j1_JVT, &b_j1_JVT);
   fChain->SetBranchAddress("jj_pt", &jj_pt, &b_jj_pt);
   fChain->SetBranchAddress("jj_eta", &jj_eta, &b_jj_eta);
   fChain->SetBranchAddress("jj_phi", &jj_phi, &b_jj_phi);
   fChain->SetBranchAddress("jj_rap", &jj_rap, &b_jj_rap);
   fChain->SetBranchAddress("jj_e", &jj_e, &b_jj_e);
   fChain->SetBranchAddress("jj_m", &jj_m, &b_jj_m);
   fChain->SetBranchAddress("jj_deta", &jj_deta, &b_jj_deta);
   fChain->SetBranchAddress("jj_dphi", &jj_dphi, &b_jj_dphi);
   fChain->SetBranchAddress("jj_dphi_signed", &jj_dphi_signed, &b_jj_dphi_signed);
   fChain->SetBranchAddress("jj_drap", &jj_drap, &b_jj_drap);
   fChain->SetBranchAddress("jj_dr", &jj_dr, &b_jj_dr);
   fChain->SetBranchAddress("j0met_dphi", &j0met_dphi, &b_j0met_dphi);
   fChain->SetBranchAddress("j1met_dphi", &j1met_dphi, &b_j1met_dphi);
   fChain->SetBranchAddress("j0gam_dr", &j0gam_dr, &b_j0gam_dr);
   fChain->SetBranchAddress("j1gam_dr", &j1gam_dr, &b_j1gam_dr);
   fChain->SetBranchAddress("j0lep_dr", &j0lep_dr, &b_j0lep_dr);
   fChain->SetBranchAddress("j1lep_dr", &j1lep_dr, &b_j1lep_dr);
   fChain->SetBranchAddress("lepgam_dr", &lepgam_dr, &b_lepgam_dr);
   fChain->SetBranchAddress("j_pt", &j_pt, &b_j_pt);
   fChain->SetBranchAddress("j_eta", &j_eta, &b_j_eta);
   fChain->SetBranchAddress("j_phi", &j_phi, &b_j_phi);
   fChain->SetBranchAddress("j_e", &j_e, &b_j_e);
   fChain->SetBranchAddress("dPhi_Wy_jj", &dPhi_Wy_jj, &b_dPhi_Wy_jj);
   fChain->SetBranchAddress("dR_Wy_jj", &dR_Wy_jj, &b_dR_Wy_jj);
   fChain->SetBranchAddress("dR_Wy", &dR_Wy, &b_dR_Wy);
   fChain->SetBranchAddress("nn_score", &nn_score, &b_nn_score);
   Notify();
}

Bool_t vbswy::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void vbswy::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t vbswy::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef vbswy_cxx
