//#define ff_tree_cxx
//#define DAOD_tree_cxx
#include <iostream>
//#include "ff_tree.h"
//#include "DAOD_tree.h"
#include <TH2.h>
#include <TFile.h>
#include <TTree.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <dirent.h>
#include <string>
#include <boost/algorithm/string.hpp>
#include <vector>
#include "TMath.h"
using namespace std;
int minitree()
{
  cout << "hello !" <<endl;
  /*
  TFile* mini_file[6]; TTree* mini_tree[6];
  TString mini_name[6] = {"data", "ZZ", "ZW", "zjets", "vbf", "wt"};
  //                       0       1      2      3      4      5

  Double_t sf, mc_weight;

  Double_t luminosity, xsection, geneff, kfactor, weight_all, sum_of_weights;

  UInt_t          runnumber;
  ULong64_t       eventnumber;
  UInt_t          dsid;
  Int_t           pass_triggermatch_e;
  Int_t           pass_triggermatch_e2;
  Int_t           pass_triggermatch_e3;
  Int_t           pass_triggermatch_mu;
  Int_t           pass_triggermatch_mu2;
  Int_t           pass_triggermatch_mu3;
  Int_t           pass_Ze1e2;
  Int_t           pass_Ze1e3;
  Int_t           pass_Ze2e3;
  Int_t           pass_Zm1m2;
  Int_t           pass_Zm1m3;
  Int_t           pass_Zm2m3;
  Int_t           pass_onegam;
  Int_t           pass_twojet;
  Int_t           pass_mjj;
  Int_t           pass_second_lepton_veto;
  Int_t           pass_trigger;
  Int_t           pass_dphi;
  Int_t           pass_dr;
  Int_t           gam_n;
  Double_t        gam_pt;
  Double_t        gam_eta;
  Double_t        gam_phi;
  Double_t        gam_e;
  Double_t        gam_xi;
  Bool_t          gam_idTight;
  Bool_t          gam_isoTight;
  Int_t           e_n;
  Double_t        e_pt;
  Double_t        e_eta;
  Double_t        e_phi;
  Double_t        e_e;
  Double_t        e_charge;
  Double_t        e2_pt;
  Double_t        e2_eta;
  Double_t        e2_phi;
  Double_t        e2_e;
  Double_t        e2_charge;
  Double_t        e3_pt;
  Double_t        e3_eta;
  Double_t        e3_phi;
  Double_t        e3_e;
  Double_t        e3_charge;
  Int_t           mu_n;
  Double_t        mu_pt;
  Double_t        mu_eta;
  Double_t        mu_phi;
  Double_t        mu_e;
  Double_t        mu_charge;
  Double_t        mu2_pt;
  Double_t        mu2_eta;
  Double_t        mu2_phi;
  Double_t        mu2_e;
  Double_t        mu2_charge;
  Double_t        mu3_pt;
  Double_t        mu3_eta;
  Double_t        mu3_phi;
  Double_t        mu3_e;
  Double_t        mu3_charge;
  Int_t           e_truth_type;
  Int_t           e_truth_origin;
  Double_t        met_x;
  Double_t        met_y;
  Double_t        met_phi;
  Double_t        met;
  Double_t        met_eta;
  Double_t        e_idLoose;
  Double_t        e_idMedium;
  Double_t        e_idTight;
  Double_t        e_isoTight;
  Double_t        e_isoLoose;
  Bool_t          e_passIP;
  Double_t        e_d0sig;
  Double_t        e_z0;
  Double_t        e2_idLoose;
  Double_t        e2_idMedium;
  Double_t        e2_idTight;
  Double_t        e2_isoTight;
  Double_t        e2_isoLoose;
  Bool_t          e2_passIP;
  Double_t        e2_d0sig;
  Double_t        e2_z0;
  Double_t        e3_idLoose;
  Double_t        e3_idMedium;
  Double_t        e3_idTight;
  Double_t        e3_isoTight;
  Double_t        e3_isoLoose;
  Bool_t          e3_passIP;
  Double_t        e3_d0sig;
  Double_t        e3_z0;
  Bool_t          mu_idLoose;
  Bool_t          mu_idMedium;
  Bool_t          mu_idTight;
  Bool_t          mu_isoFCLoose;
  Bool_t          mu_isoFCTight;
  Bool_t          mu_passIP;
  Double_t        mu_d0sig;
  Double_t        mu_z0;
  Bool_t          mu2_idLoose;
  Bool_t          mu2_idMedium;
  Bool_t          mu2_idTight;
  Bool_t          mu2_isoFCLoose;
  Bool_t          mu2_isoFCTight;
  Bool_t          mu2_passIP;
  Double_t        mu2_d0sig;
  Double_t        mu2_z0;
  Bool_t          mu3_idLoose;
  Bool_t          mu3_idMedium;
  Bool_t          mu3_idTight;
  Bool_t          mu3_isoFCLoose;
  Bool_t          mu3_isoFCTight;
  Bool_t          mu3_passIP;
  Double_t        mu3_d0sig;
  Double_t        mu3_z0;
  Int_t           mu_truth_type;
  Int_t           mu_truth_origin;
  Int_t           n_bjets;
  Int_t           n_gapjets;
  Double_t        jj_m;
  Double_t        jj_dr;
  Double_t        j0_pt;
  Double_t        j1_pt;

  Double_t w_mt_m;
  Double_t w_mt_m2;
  Double_t w_mt_m3;
  Double_t w_mt_e;
  Double_t w_mt_e2;
  Double_t w_mt_e3;

  Double_t Ze1e2mass = 0;
  Double_t Ze1e3mass = 0;
  Double_t Ze2e3mass = 0;
  Double_t Zm1m2mass = 0.;
  Double_t Zm1m3mass = 0.;
  Double_t Zm2m3mass = 0.;

  Int_t e2_truth_type = -1;
  Int_t e2_truth_origin = -1;
  Int_t e3_truth_type = -1;
  Int_t e3_truth_origin = -1;

  Int_t mu2_truth_type = -1;
  Int_t mu2_truth_origin = -1;
  Int_t mu3_truth_type = -1;
  Int_t mu3_truth_origin = -1;

  Int_t lep_n = 0;
  Int_t lep_n_truth = 0;
  Int_t e_n_truth = 0;
  Int_t mu_n_truth = 0;

  //TString dir_out = "/publicfs/atlas/atlasnew/SM/VBS/xiw/mini_files/zjets_v10_mc16e/"; //end with /
  TString dir_out = "/publicfs/atlas/atlasnew/SM/VBS/xiw/mini_files/zjets_v20/"; //end with /
  vector<TString> dir_in;
  //dir_in.push_back("/publicfs/atlas/atlasnew/SM/VBS/xiw/download_merge/zjets_ntuple_v10/"); // end with /
  dir_in.push_back("/publicfs/atlas/atlasnew/SM/VBS/xiw/download_merge/zjets_ntuple_v20/"); // end with /
  if(system("mkdir -p "+dir_out)) return 0; 
  for (int i=0; i<6; i++) {
    mini_file[i] = TFile::Open(dir_out+mini_name[i]+".root", "recreate");
    mini_tree[i] = new TTree("vbswy", "vbswy");

    // event basic info
    mini_tree[i]->Branch("runnumber", &runnumber);
    mini_tree[i]->Branch("eventnumber", &eventnumber);
    mini_tree[i]->Branch("dsid", &dsid);

    // weights
    mini_tree[i]->Branch("pass_triggermatch_e", &pass_triggermatch_e);
    mini_tree[i]->Branch("pass_triggermatch_e2", &pass_triggermatch_e2);
    mini_tree[i]->Branch("pass_triggermatch_e3", &pass_triggermatch_e3);
    mini_tree[i]->Branch("pass_triggermatch_mu", &pass_triggermatch_mu);
    mini_tree[i]->Branch("pass_triggermatch_mu2", &pass_triggermatch_mu2);
    mini_tree[i]->Branch("pass_triggermatch_mu3", &pass_triggermatch_mu3);
    mini_tree[i]->Branch("pass_Ze1e2", &pass_Ze1e2);
    mini_tree[i]->Branch("pass_Ze1e3", &pass_Ze1e3);
    mini_tree[i]->Branch("pass_Ze2e3", &pass_Ze2e3);
    mini_tree[i]->Branch("pass_Zm1m2", &pass_Zm1m2);
    mini_tree[i]->Branch("pass_Zm1m3", &pass_Zm1m3);
    mini_tree[i]->Branch("pass_Zm2m3", &pass_Zm2m3);

    mini_tree[i]->Branch("pass_onegam", &pass_onegam);
    mini_tree[i]->Branch("pass_twojet", &pass_twojet);
    mini_tree[i]->Branch("pass_mjj", &pass_mjj);
    mini_tree[i]->Branch("pass_second_lepton_veto", &pass_second_lepton_veto);
    mini_tree[i]->Branch("pass_trigger", &pass_trigger);
    mini_tree[i]->Branch("pass_dphi", &pass_dphi);
    mini_tree[i]->Branch("pass_dr", &pass_dr);
    mini_tree[i]->Branch("gam_n", &gam_n);
    mini_tree[i]->Branch("gam_pt", &gam_pt);
    mini_tree[i]->Branch("gam_eta", &gam_eta);
    mini_tree[i]->Branch("gam_phi", &gam_phi);
    mini_tree[i]->Branch("gam_e", &gam_e);
    mini_tree[i]->Branch("gam_xi", &gam_xi);
    mini_tree[i]->Branch("gam_idTight",&gam_idTight);
    mini_tree[i]->Branch("gam_isoTight",&gam_isoTight);

    mini_tree[i]->Branch("e_n", &e_n);
    mini_tree[i]->Branch("e_pt", &e_pt);
    mini_tree[i]->Branch("e_eta", &e_eta);
    mini_tree[i]->Branch("e_phi", &e_phi);
    mini_tree[i]->Branch("e_e", &e_e);
    mini_tree[i]->Branch("e_charge", &e_charge);
    mini_tree[i]->Branch("e2_pt", &e2_pt);
    mini_tree[i]->Branch("e2_eta", &e2_eta);
    mini_tree[i]->Branch("e2_phi", &e2_phi);
    mini_tree[i]->Branch("e2_e", &e2_e);
    mini_tree[i]->Branch("e2_charge", &e2_charge);
    mini_tree[i]->Branch("e3_pt", &e3_pt);
    mini_tree[i]->Branch("e3_eta", &e3_eta);
    mini_tree[i]->Branch("e3_phi", &e3_phi);
    mini_tree[i]->Branch("e3_e", &e3_e);
    mini_tree[i]->Branch("e3_charge", &e3_charge);
    mini_tree[i]->Branch("mu_n", &mu_n);
    mini_tree[i]->Branch("mu_pt", &mu_pt);
    mini_tree[i]->Branch("mu_eta", &mu_eta);
    mini_tree[i]->Branch("mu_phi", &mu_phi);
    mini_tree[i]->Branch("mu_e", &mu_e);
    mini_tree[i]->Branch("mu_charge", &mu_charge);
    mini_tree[i]->Branch("mu2_pt", &mu2_pt);
    mini_tree[i]->Branch("mu2_eta", &mu2_eta);
    mini_tree[i]->Branch("mu2_phi", &mu2_phi);
    mini_tree[i]->Branch("mu2_e", &mu2_e);
    mini_tree[i]->Branch("mu2_charge", &mu2_charge);
    mini_tree[i]->Branch("mu3_pt", &mu3_pt);
    mini_tree[i]->Branch("mu3_eta", &mu3_eta);
    mini_tree[i]->Branch("mu3_phi", &mu3_phi);
    mini_tree[i]->Branch("mu3_e", &mu3_e);
    mini_tree[i]->Branch("mu3_charge", &mu3_charge);

    mini_tree[i]->Branch("e_truth_type", &e_truth_type);
    mini_tree[i]->Branch("e_truth_origin", &e_truth_origin);
    mini_tree[i]->Branch("met_x", &met_x);
    mini_tree[i]->Branch("met_y", &met_y);
    mini_tree[i]->Branch("met_phi", &met_phi);
    mini_tree[i]->Branch("met", &met);
    mini_tree[i]->Branch("met_eta", &met_eta);

    mini_tree[i]->Branch("e_idLoose",&e_idLoose);
    mini_tree[i]->Branch("e_idMedium",&e_idMedium);
    mini_tree[i]->Branch("e_idTight",&e_idTight);
    mini_tree[i]->Branch("e_isoTight",&e_isoTight);
    mini_tree[i]->Branch("e_isoLoose",&e_isoLoose);
    mini_tree[i]->Branch("e_passIP",&e_passIP);
    mini_tree[i]->Branch("e_d0sig",&e_d0sig);
    mini_tree[i]->Branch("e_z0",&e_z0);
    mini_tree[i]->Branch("e2_idLoose",&e2_idLoose);
    mini_tree[i]->Branch("e2_idMedium",&e2_idMedium);
    mini_tree[i]->Branch("e2_idTight",&e2_idTight);
    mini_tree[i]->Branch("e2_isoTight",&e2_isoTight);
    mini_tree[i]->Branch("e2_isoLoose",&e2_isoLoose);
    mini_tree[i]->Branch("e2_passIP",&e2_passIP);
    mini_tree[i]->Branch("e2_d0sig",&e2_d0sig);
    mini_tree[i]->Branch("e2_z0",&e2_z0);
    mini_tree[i]->Branch("e3_idLoose",&e3_idLoose);
    mini_tree[i]->Branch("e3_idMedium",&e3_idMedium);
    mini_tree[i]->Branch("e3_idTight",&e3_idTight);
    mini_tree[i]->Branch("e3_isoTight",&e3_isoTight);
    mini_tree[i]->Branch("e3_isoLoose",&e3_isoLoose);
    mini_tree[i]->Branch("e3_passIP",&e3_passIP);
    mini_tree[i]->Branch("e3_d0sig",&e3_d0sig);
    mini_tree[i]->Branch("e3_z0",&e3_z0);

    mini_tree[i]->Branch("mu_idLoose",&mu_idLoose);
    mini_tree[i]->Branch("mu_idMedium",&mu_idMedium);
    mini_tree[i]->Branch("mu_idTight",&mu_idTight);
    mini_tree[i]->Branch("mu_isoFCLoose",&mu_isoFCLoose);
    mini_tree[i]->Branch("mu_isoFCTight",&mu_isoFCTight);
    mini_tree[i]->Branch("mu_passIP",&mu_passIP);
    mini_tree[i]->Branch("mu_d0sig",&mu_d0sig);
    mini_tree[i]->Branch("mu_z0",&mu_z0);
    mini_tree[i]->Branch("mu2_idLoose",&mu2_idLoose);
    mini_tree[i]->Branch("mu2_idMedium",&mu2_idMedium);
    mini_tree[i]->Branch("mu2_idTight",&mu2_idTight);
    mini_tree[i]->Branch("mu2_isoFCLoose",&mu2_isoFCLoose);
    mini_tree[i]->Branch("mu2_isoFCTight",&mu2_isoFCTight);
    mini_tree[i]->Branch("mu2_passIP",&mu2_passIP);
    mini_tree[i]->Branch("mu2_d0sig",&mu2_d0sig);
    mini_tree[i]->Branch("mu2_z0",&mu2_z0);
    mini_tree[i]->Branch("mu3_idLoose",&mu3_idLoose);
    mini_tree[i]->Branch("mu3_idMedium",&mu3_idMedium);
    mini_tree[i]->Branch("mu3_idTight",&mu3_idTight);
    mini_tree[i]->Branch("mu3_isoFCLoose",&mu3_isoFCLoose);
    mini_tree[i]->Branch("mu3_isoFCTight",&mu3_isoFCTight);
    mini_tree[i]->Branch("mu3_passIP",&mu3_passIP);
    mini_tree[i]->Branch("mu3_d0sig",&mu3_d0sig);
    mini_tree[i]->Branch("mu3_z0",&mu3_z0);
    mini_tree[i]->Branch("mu_truth_type", &mu_truth_type);
    mini_tree[i]->Branch("mu_truth_origin", &mu_truth_origin);
    mini_tree[i]->Branch("n_bjets",&n_bjets);
    mini_tree[i]->Branch("n_gapjets",&n_gapjets);
    mini_tree[i]->Branch("jj_m", &jj_m);
    mini_tree[i]->Branch("jj_dr", &jj_dr);
    mini_tree[i]->Branch("j0_pt", &j0_pt);
    mini_tree[i]->Branch("j1_pt", &j1_pt);

    mini_tree[i]->Branch("sf", &sf);
    mini_tree[i]->Branch("mc_weight", &mc_weight);
    mini_tree[i]->Branch("luminosity", &luminosity);
    mini_tree[i]->Branch("xsection", &xsection);
    mini_tree[i]->Branch("geneff", &geneff);
    mini_tree[i]->Branch("kfactor", &kfactor);
    mini_tree[i]->Branch("weight_all", &weight_all);
    mini_tree[i]->Branch("sum_of_weights", &sum_of_weights);  
 
    mini_tree[i]->Branch("w_mt_e", &w_mt_e); 
    mini_tree[i]->Branch("w_mt_e2", &w_mt_e2); 
    mini_tree[i]->Branch("w_mt_e3", &w_mt_e3); 
    mini_tree[i]->Branch("w_mt_m", &w_mt_m); 
    mini_tree[i]->Branch("w_mt_m2", &w_mt_m2); 
    mini_tree[i]->Branch("w_mt_m3", &w_mt_m3); 

    mini_tree[i]->Branch("Ze1e2mass", &Ze1e2mass); 
    mini_tree[i]->Branch("Ze1e3mass", &Ze1e3mass); 
    mini_tree[i]->Branch("Ze2e3mass", &Ze2e3mass); 
    mini_tree[i]->Branch("Zm1m2mass", &Zm1m2mass); 
    mini_tree[i]->Branch("Zm1m3mass", &Zm1m3mass); 
    mini_tree[i]->Branch("Zm2m3mass", &Zm2m3mass); 

    mini_tree[i]->Branch("e2_truth_type", &e2_truth_type); 
    mini_tree[i]->Branch("e3_truth_type", &e3_truth_type); 
    mini_tree[i]->Branch("e2_truth_origin", &e2_truth_origin); 
    mini_tree[i]->Branch("e3_truth_origin", &e3_truth_origin); 

    mini_tree[i]->Branch("mu2_truth_type", &mu2_truth_type);
    mini_tree[i]->Branch("mu2_truth_origin", &mu2_truth_origin);
    mini_tree[i]->Branch("mu3_truth_type", &mu3_truth_type);
    mini_tree[i]->Branch("mu3_truth_origin", &mu3_truth_origin);

    mini_tree[i]->Branch("lep_n", &lep_n);
    mini_tree[i]->Branch("lep_n_truth", &lep_n_truth);
    mini_tree[i]->Branch("e_n_truth", &e_n_truth);
    mini_tree[i]->Branch("mu_n_truth", &mu_n_truth);

  }
  for (int di = 0; di< dir_in.size(); di++){
    if (auto dir = opendir(dir_in[di])) {
      while (auto f = readdir(dir)) {
        if (!f->d_name || f->d_name[0] == '.')
            continue; // Skip everything that starts with a dot
        TString f_name = f->d_name;
//------------------
       // std::string dsid_str1 = f->d_name;
       // if (!(boost::algorithm::contains(dsid_str1, "VV"))) continue;
//----------------
        printf("File: %s\n", f->d_name);
      }
    }
  }      


  for (int di = 0; di< dir_in.size(); di++){
    if (auto dir = opendir(dir_in[di])) {
      while (auto f = readdir(dir)) {
        if (!f->d_name || f->d_name[0] == '.')
            continue; // Skip everything that starts with a dot
        TString f_name = f->d_name;
//------------------
       // std::string dsid_str1 = f->d_name;
       // if (!(boost::algorithm::contains(dsid_str1, "VV"))) continue;
//----------------



        printf("File: %s\n", f->d_name);
        
        TFile *large_file = TFile::Open(dir_in[di]+f_name,"READ"); //TFile* myfile = new TFile("","OPEN");
        TTree *fChain_vbswy=(TTree*)large_file->Get("ff_tree");
        TTree *fChain_DAOD=(TTree*)large_file->Get("DAOD_tree");
        DAOD_tree large_DAOD(fChain_DAOD);//  lage_DAOD.Loop(); 
        ff_tree large_vbswy(fChain_vbswy); // large_vbswy.Loop();
            
        Long64_t nentries_vbswy = fChain_vbswy->GetEntriesFast();
        Long64_t nentries_DAOD = fChain_DAOD->GetEntriesFast();
        Float_t lumi=0, xsec=0, geff=0, kfac=0, sw=0;
        for (Long64_t jentry=0; jentry<nentries_DAOD;jentry++) {
          fChain_DAOD->GetEntry(jentry);
          if (jentry==0) {
            lumi = large_DAOD.luminosity;
            xsec = large_DAOD.xsection;
            geff = large_DAOD.geneff;
            kfac = large_DAOD.kfactor;
          }
          sw += large_DAOD.sum_of_weights;
        }
        for (Long64_t jentry=0; jentry<nentries_vbswy;jentry++) {
          fChain_vbswy->GetEntry(jentry);

          luminosity = lumi; xsection = xsec; geneff = geff; kfactor = kfac; weight_all = large_vbswy.weight_all; sum_of_weights = sw;
          sf = lumi*xsec*geff*kfac/sw*large_vbswy.weight_e*large_vbswy.weight_all*large_vbswy.weight_m;
          mc_weight = large_vbswy.weight_e*large_vbswy.weight_all*large_vbswy.weight_m;

          runnumber = large_vbswy.runnumber;
          eventnumber = large_vbswy.eventnumber;
          dsid =large_vbswy.dsid;
          pass_triggermatch_e=large_vbswy.pass_triggermatch_e;
          pass_triggermatch_e2=large_vbswy.pass_triggermatch_e2;
          pass_triggermatch_e3=large_vbswy.pass_triggermatch_e3;
          pass_triggermatch_mu=large_vbswy.pass_triggermatch_mu;
          pass_triggermatch_mu2=large_vbswy.pass_triggermatch_mu2;
          pass_triggermatch_mu3=large_vbswy.pass_triggermatch_mu3;
          pass_Ze1e2=large_vbswy.pass_Ze1e2;
          pass_Ze1e3=large_vbswy.pass_Ze1e3;
          pass_Ze2e3=large_vbswy.pass_Ze2e3;
          pass_Zm1m2=large_vbswy.pass_Zm1m2;
          pass_Zm1m3=large_vbswy.pass_Zm1m3;
          pass_Zm2m3=large_vbswy.pass_Zm2m3;
          pass_onegam=large_vbswy.pass_onegam;
          pass_twojet=large_vbswy.pass_twojet;
          pass_mjj=large_vbswy.pass_mjj;
          pass_second_lepton_veto=large_vbswy.pass_second_lepton_veto;
          pass_trigger=large_vbswy.pass_trigger;
          pass_dphi=large_vbswy.pass_dphi;
          pass_dr=large_vbswy.pass_dr;
          gam_n=large_vbswy.gam_n;
          gam_pt=large_vbswy.gam_pt;
          gam_eta=large_vbswy.gam_eta;
          gam_phi=large_vbswy.gam_phi;
          gam_e=large_vbswy.gam_e;
          gam_xi=large_vbswy.gam_xi;
          gam_idTight=large_vbswy.gam_idTight;
          gam_isoTight=large_vbswy.gam_isoTight;
          e_n=large_vbswy.e_n;
          e_pt=large_vbswy.e_pt;
          e_eta=large_vbswy.e_eta;
          e_phi=large_vbswy.e_phi;
          e_e=large_vbswy.e_e;
          e_charge=large_vbswy.e_charge;
          e2_pt=large_vbswy.e2_pt;
          e2_eta=large_vbswy.e2_eta;
          e2_phi=large_vbswy.e2_phi;
          e2_e=large_vbswy.e2_e;
          e2_charge=large_vbswy.e2_charge;
          e3_pt=large_vbswy.e3_pt;
          e3_eta=large_vbswy.e3_eta;
          e3_phi=large_vbswy.e3_phi;
          e3_e=large_vbswy.e3_e;
          e3_charge=large_vbswy.e3_charge;
          mu_n=large_vbswy.mu_n;
          mu_pt=large_vbswy.mu_pt;
          mu_eta=large_vbswy.mu_eta;
          mu_phi=large_vbswy.mu_phi;
          mu_e=large_vbswy.mu_e;
          mu_charge=large_vbswy.mu_charge;
          mu2_pt=large_vbswy.mu2_pt;
          mu2_eta=large_vbswy.mu2_eta;
          mu2_phi=large_vbswy.mu2_phi;
          mu2_e=large_vbswy.mu2_e;
          mu2_charge=large_vbswy.mu2_charge;
          mu3_pt=large_vbswy.mu3_pt;
          mu3_eta=large_vbswy.mu3_eta;
          mu3_phi=large_vbswy.mu3_phi;
          mu3_e=large_vbswy.mu3_e;
          mu3_charge=large_vbswy.mu3_charge;
          e_truth_type=large_vbswy.e_truth_type;
          e_truth_origin=large_vbswy.e_truth_origin;
          met_x=large_vbswy.met_x;
          met_y=large_vbswy.met_y;
          met_phi=large_vbswy.met_phi;
          met=large_vbswy.met;
          met_eta=large_vbswy.met_eta;
          e_idLoose=large_vbswy.e_idLoose;
          e_idMedium=large_vbswy.e_idMedium;
          e_idTight=large_vbswy.e_idTight;
          e_isoTight=large_vbswy.e_isoTight;
          e_isoLoose=large_vbswy.e_isoLoose;
          e_passIP=large_vbswy.e_passIP;
          e_d0sig=large_vbswy.e_d0sig;
          e_z0=large_vbswy.e_z0;
          e2_idLoose=large_vbswy.e2_idLoose;
          e2_idMedium=large_vbswy.e2_idMedium;
          e2_idTight=large_vbswy.e2_idTight;
          e2_isoTight=large_vbswy.e2_isoTight;
          e2_isoLoose=large_vbswy.e2_isoLoose;
          e2_passIP=large_vbswy.e2_passIP;
          e2_d0sig=large_vbswy.e2_d0sig;
          e2_z0=large_vbswy.e2_z0;
          e3_idLoose=large_vbswy.e3_idLoose;
          e3_idMedium=large_vbswy.e3_idMedium;
          e3_idTight=large_vbswy.e3_idTight;
          e3_isoTight=large_vbswy.e3_isoTight;
          e3_isoLoose=large_vbswy.e3_isoLoose;
          e3_passIP=large_vbswy.e3_passIP;
          e3_d0sig=large_vbswy.e3_d0sig;
          e3_z0=large_vbswy.e3_z0;
          mu_idLoose=large_vbswy.mu_idLoose;
          mu_idMedium=large_vbswy.mu_idMedium;
          mu_idTight=large_vbswy.mu_idTight;
          mu_isoFCLoose=large_vbswy.mu_isoFCLoose;
          mu_isoFCTight=large_vbswy.mu_isoFCTight;
          mu_passIP=large_vbswy.mu_passIP;
          mu_d0sig=large_vbswy.mu_d0sig;
          mu_z0=large_vbswy.mu_z0;
          mu2_idLoose=large_vbswy.mu2_idLoose;
          mu2_idMedium=large_vbswy.mu2_idMedium;
          mu2_idTight=large_vbswy.mu2_idTight;
          mu2_isoFCLoose=large_vbswy.mu2_isoFCLoose;
          mu2_isoFCTight=large_vbswy.mu2_isoFCTight;
          mu2_passIP=large_vbswy.mu2_passIP;
          mu2_d0sig=large_vbswy.mu2_d0sig;
          mu2_z0=large_vbswy.mu2_z0;
          mu3_idLoose=large_vbswy.mu3_idLoose;
          mu3_idMedium=large_vbswy.mu3_idMedium;
          mu3_idTight=large_vbswy.mu3_idTight;
          mu3_isoFCLoose=large_vbswy.mu3_isoFCLoose;
          mu3_isoFCTight=large_vbswy.mu3_isoFCTight;
          mu3_passIP=large_vbswy.mu3_passIP;
          mu3_d0sig=large_vbswy.mu3_d0sig;
          mu3_z0=large_vbswy.mu3_z0;
          mu_truth_type=large_vbswy.mu_truth_type;
          mu_truth_origin=large_vbswy.mu_truth_origin;
          n_bjets=large_vbswy.n_bjets;
          n_gapjets=large_vbswy.n_gapjets;
          jj_m=large_vbswy.jj_m;
          jj_dr=large_vbswy.jj_dr;
          j0_pt=large_vbswy.j0_pt;
          j1_pt=large_vbswy.j1_pt;

          Ze1e2mass        = large_vbswy.Ze1e2mass;
          Ze1e3mass        = large_vbswy.Ze1e3mass;
          Ze2e3mass        = large_vbswy.Ze2e3mass;
          Zm1m2mass        = large_vbswy.Zm1m2mass;
          Zm1m3mass        = large_vbswy.Zm1m3mass;
          Zm2m3mass        = large_vbswy.Zm2m3mass;

          e2_truth_type    = large_vbswy.e2_truth_type;
          e2_truth_origin  = large_vbswy.e2_truth_origin;
          e3_truth_type    = large_vbswy.e3_truth_type; 
          e3_truth_origin  = large_vbswy.e3_truth_origin;

          mu2_truth_type   = large_vbswy.mu2_truth_type;
          mu2_truth_origin = large_vbswy.mu2_truth_origin;
          mu3_truth_type   = large_vbswy.mu3_truth_type;
          mu3_truth_origin = large_vbswy.mu3_truth_origin;
          
          lep_n            = large_vbswy.lep_n;        
          lep_n_truth      = large_vbswy.lep_n_truth; 
          e_n_truth        = large_vbswy.e_n_truth;    
          mu_n_truth       = large_vbswy.mu_n_truth;   



          w_mt_e = TMath::Sqrt(2*(e_pt)*(TMath::Abs(met))*(1-TMath::Cos(TMath::Abs((met_phi)-(e_phi)))));
          w_mt_e2 = TMath::Sqrt(2*(e2_pt)*(TMath::Abs(met))*(1-TMath::Cos(TMath::Abs((met_phi)-(e2_phi)))));
          w_mt_e3 = TMath::Sqrt(2*(e3_pt)*(TMath::Abs(met))*(1-TMath::Cos(TMath::Abs((met_phi)-(e3_phi)))));
          w_mt_m = TMath::Sqrt(2*(mu_pt)*(TMath::Abs(met))*(1-TMath::Cos(TMath::Abs((met_phi)-(mu_phi)))));
          w_mt_m2 = TMath::Sqrt(2*(mu2_pt)*(TMath::Abs(met))*(1-TMath::Cos(TMath::Abs((met_phi)-(mu2_phi)))));
          w_mt_m3 = TMath::Sqrt(2*(mu3_pt)*(TMath::Abs(met))*(1-TMath::Cos(TMath::Abs((met_phi)-(mu3_phi)))));

          std::string fileName_str = f->d_name;
           //if (mc_str.contain("data")) {
          if (boost::algorithm::contains(fileName_str, "period")) {
            //cout<<"lumi, xsec, kfac, geff, sw " << lumi<<"  "<<xsec <<"  " << kfac <<"  " << geff <<"  "<< sw << "  " <<endl; 
            sf = large_vbswy.weight_e*large_vbswy.weight_all*large_vbswy.weight_m;
            mini_tree[0]->Fill();  
          }  
          else {
            //cout<<"lumi, xsec, kfac, geff, sw, sf, weight_all " << lumi<<"  "<<xsec <<"  " << kfac <<"  " << geff <<"  "<< sw << "  " <<sf<<""<<large_vbswy.weight_all<<endl; 
            if (dsid == 364250) 
              mini_tree[1]->Fill();
            else if (dsid == 364253) 
              mini_tree[2]->Fill();
            else if(dsid >= 364100 && dsid <= 364141) 
              mini_tree[3]->Fill();
            else if((dsid >= 308096 && dsid <= 308098)||(dsid >= 308092 && dsid <= 308094)) 
              mini_tree[4]->Fill();
            else 
              mini_tree[5]->Fill();
          }
        }
        large_file->Close();
      }
      closedir(dir);
    }
  }
  for (int i=0; i<6; i++) {
    mini_file[i]->cd();
   mini_tree[i]->Write();
   mini_file[i]->Close();
  }
  */
  return 0;
}
